package com.selenium.base;
/**
 * 
 * @author Shenll Technologies Solutions
 *
 */
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.model.GetExcelInput;
import com.model.Constants.EXCEL_METHODS_INPUT;

public class BaseClass {
	protected WebDriver driver;
	public void setUp() throws Exception {
		 GetExcelInput getInput = new GetExcelInput();
		 String Browser_Firefox = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Firefox);
		 String Browser_Chrome = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Chrome);
		 String Browser_Safari = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Safari);
		
		  if(Browser_Chrome.contains("Y")){  
	        	//System.setProperty("webdriver.chrome.driver","Path of &lt;chromedriver.exe&gt;");
	     		System.setProperty("webdriver.chrome.driver",".\\chromedriver.exe");
			//  System.setProperty("webdriver.chrome.driver","/var/lib/jenkins/workspace/moneysmartfirst/chromedriver.exe");
	             ChromeOptions options = new ChromeOptions();
	            // options.addArguments("--test-type");
	             driver = new ChromeDriver(options);
	         } 
		  else if(Browser_Firefox.contains("Y")){
			    driver = new FirefoxDriver();
			    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			    driver.manage().window().maximize();
         }
         else
         {
        	   driver = new SafariDriver();
         }
	}
}
