package com.common.utility;


/**
 * 
 * @author Shenll Technology Solutions
 *
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.net.ftp.FTPClient;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class SendEmailTask {
	/*
	 * Email send status  
	 */
	private static final int FAILURE_CODE = 0;
	private static final int SUCCESS_CODE = 1;
	private static final String SUCCESS_MESSAGE = "Email sent successfully.";
	private static final String FAILURE_MESSAGE = "Failed to send email";
	private static final String EXCEPTION_MESSAGE = "Exception while sending email : ";
	
	private String emailSendingStatus = "Email sending status";
	
	
	
	private String ATTACHMENT_CONTENT_TYPE = "text/html";
	private String ATTACHMENT_OUTPUT_FOLDER_NAME = "test-output/";
	
	public String send(SendEmailTaskListener emailSendlistener, String sendingAttachmentFileName, 
			final String AUTH_EMAIL, final String AUTH_EMAIL_PASSWORD,
			String FROM_USERNAME, String TO_USERNAME,
			String FROM_EMAIL, String TO_EMAIL,
			String EMAIL_SUBJECT, String EMAIL_BODY_MESSAGE,String CC_USERNAME, String CC_EMAIL)
	{
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(new File("settings.properties")));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Session session = Session.getDefaultInstance(props,
				new Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								AUTH_EMAIL, AUTH_EMAIL_PASSWORD);
					}
				});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(FROM_EMAIL));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(TO_EMAIL) );
			message.setRecipients(Message.RecipientType.CC,InternetAddress.parse(CC_EMAIL));
			
			message.setSubject(EMAIL_SUBJECT);
			BodyPart body = new MimeBodyPart();
			Configuration cfg = new Configuration();
			Template template;
			try {
				template = cfg.getTemplate(ATTACHMENT_OUTPUT_FOLDER_NAME+sendingAttachmentFileName);
				
				Map<String, String> rootMap = new HashMap<String, String>();
				rootMap.put("to", TO_USERNAME);
				rootMap.put("body", EMAIL_BODY_MESSAGE);
				rootMap.put("from", FROM_USERNAME);
			
				
				System.out.println("CC_USERNAME...."+CC_USERNAME);
				System.out.println("CC_EMAIL...."+CC_EMAIL);
				
				rootMap.put("cc", CC_EMAIL);
				
				Writer out = new StringWriter();
				try {
					template.process(rootMap, out);
				} catch (TemplateException e) {
					e.printStackTrace();
				}
				body.setContent(out.toString(), ATTACHMENT_CONTENT_TYPE);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(body);
			body = new MimeBodyPart();
			String filename = "hello.txt";
			DataSource source = new FileDataSource(filename);
			body.setDataHandler(new DataHandler(source));
			body.setFileName(filename);
			
			message.setContent(multipart);
			Transport.send(message);
			
			System.out.println("Sending email....");
			emailSendingStatus = SUCCESS_MESSAGE;
			emailSendlistener.sendEmailSuccessListener(SUCCESS_CODE,emailSendingStatus);
		} catch (MessagingException e) {
			e.printStackTrace();
			emailSendingStatus = EXCEPTION_MESSAGE+e.getMessage();
			emailSendlistener.sendEmailFailureListener(FAILURE_CODE,emailSendingStatus);
		}

		return emailSendingStatus;
	}
}
