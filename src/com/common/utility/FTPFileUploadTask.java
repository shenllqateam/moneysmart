package com.common.utility;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */
	
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
 


import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
 
/**
 * A program that demonstrates how to upload files from local computer
 * to a remote FTP server using Apache Commons Net API.
 * @author www.codejava.net
 */
public class FTPFileUploadTask {
	
	private static final int FAILURE_CODE = 0;
	private static final int SUCCESS_CODE = 1;
	private static final String SUCCESS_MESSAGE = "File is uploaded successfully.";
	private static final String FAILURE_MESSAGE = "File uploading task failed";
	private static final String EXCEPTION_MESSAGE = "Exception during upload : ";
			
	/*
	 * FTP credentials. Please keep these credentials  securely
	 */
	
	private String server = "xxxx";
	private int port = 21;
	private String user = "xxxx";
	private String pass = "xxxxx";
	private String workingDirectory = "/MoneySmartTesting";
	
	
	
	private String uploadStatus = "File upload status";
  
    public String uploadThisFileToServer(FTPFileUploadListener fileUploadListener, String uploadFilePath, String uploadFileName) {
    
        FTPClient ftpClient = new FTPClient();
        try {
 
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
 
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
 
            // Changes working directory
            boolean success = ftpClient.changeWorkingDirectory(workingDirectory);
            showServerReply(ftpClient);

            if (success) {
                System.out.println("Successfully changed working directory.");
            } else {
                System.out.println("Failed to change working directory. See server's reply.");
            }
            
            // APPROACH #1: uploads first file using an InputStream
            File firstLocalFile = new File(uploadFilePath+uploadFileName);
 
            InputStream inputStream = new FileInputStream(firstLocalFile);
 
            System.out.println("Uload File Path : "+uploadFilePath);
            System.out.println("File uploading task started...");
            
            boolean done = ftpClient.storeFile(uploadFileName, inputStream);
            inputStream.close();
            if (done) {
            	uploadStatus= SUCCESS_MESSAGE;
            	fileUploadListener.uploadTaskSuccessListener(SUCCESS_CODE,uploadStatus);
            }
            else{
            	
            }
            
            System.out.println("Upload Status message : "+uploadStatus);
            
            
 
        } catch (IOException ex) {
        	System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        	uploadStatus= EXCEPTION_MESSAGE+ex.getMessage();
        	fileUploadListener.uploadTaskFailureListener(FAILURE_CODE,uploadStatus);
           
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        return uploadStatus;
    }

	private static void showServerReply(FTPClient ftpClient) {
        String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                System.out.println("SERVER: " + aReply);
            }
        }
    }
}