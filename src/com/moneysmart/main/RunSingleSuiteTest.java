
package com.moneysmart.main;
/**
 * 
 * @author Shenll Technologies Solutions
 *
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import mx4j.log.Log;

import org.testng.xml.Parser;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.testng.TestNG;

import com.moneysmart.channel.test.Filters;
import com.moneysmart.channel.test.BLogin;
import com.moneysmart.channel.test.AMasterHead;
import com.moneysmart.channel.test.ProductListValidation;
import com.moneysmart.channel.test.ProductValidation;
import com.moneysmart.channel.test.ZDetails;
import com.testing.utility.InvocationMethod;
import com.testing.utility.TestNGCustomReportListener;

import freemarker.core.CommandLine;




public class RunSingleSuiteTest {

	/**
	 * This method will start the TestNG Run dynamically
	 * @param calculationMethodInvocationCount
	 * @param filterMethodInvocationCount
	 * @param productValidationInvocationCount
	 * @param channelName
	 * @throws IOException
	 */
	public  void SingleSuit(int calculationMethodInvocationCount, int filterMethodInvocationCount,
			int productValidationInvocationCount,
			String channelName) throws IOException
	{
		 TestNGCustomReportListener tla = new TestNGCustomReportListener();
		 @SuppressWarnings("deprecation")
		 TestNG testng = new TestNG();
         @SuppressWarnings("rawtypes")
         
        
         
         Class[] classes;
         if(channelName.contains("Savings")){
             //classes = new Class[]{AMasterHead.class};
             classes = new Class[]{AMasterHead.class,ProductListValidation.class,ZDetails.class};
            }
            else if(channelName.contains("Car Loan") || channelName.contains("Fixed Deposit")){  
             classes = new Class[]{AMasterHead.class,Filters.class,ProductListValidation.class};
            // classes = new Class[]{AMasterHead.class};
            }
            else
           
            {
            classes = new Class[]{AMasterHead.class,ProductListValidation.class,ZDetails.class};
            // classes = new Class[]{AMasterHead.class,CerberusProductListValidation.class};
             //classes = new Class[]{AMasterHead.class,ProductListValidation.class,ZDetails.class};
            }
		
        
         testng.setTestClasses(classes);
         testng.addListener(tla);
         testng.setAnnotationTransformer(new InvocationMethod(calculationMethodInvocationCount,filterMethodInvocationCount,
        		 												productValidationInvocationCount));
         
         testng.run();
    }
}
