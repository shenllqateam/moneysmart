package com.moneysmart.channel.test;
/**
* 
* @author Shenll Technology Solutions
*
*/
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.common.utility.WebserviceRequest;
import com.common.utility.WebserviceRequestListener;
import com.model.Constants;
import com.model.ExcelCalculationInputData;
import com.model.ExcelInputData;
import com.model.ExcelProductValidationInputData;
import com.model.GetExcelInput;
import com.model.Constants.EXCEL_METHODS_INPUT;
import com.model.Constants.TEST_RESULT;
import com.testing.utility.GetWebElement;

public class ProductValidation implements WebserviceRequestListener {



 /*
  * Global Variables Declarations
  */
 private WebserviceRequestListener webserviceListener;
 private boolean isAllProductNameValidatedAndSucess = true;

 int totalCalculationCount = 0;
 int calculationMethodTestRunCount = 0;
 int productValidationMethodInvocationCount = 0;
 String SuccessMessage = "Product validated successfully";
 String SuccessComments = "Product in web Page and Api are equal";
 String FailureMessage = "Product in web Page and Api are not equal";
 String FailureComments = "Product in web Page and Api are not equal";
 String locatorNameForNLFFILtercalculateButton = "calculate-btn-1";
 String locatorNameForNLFFILterFieldValidation = "validation-message";
 String productValidationExpectedResultAll = "";
 String productValidationDescriptionAll = "";
 String productValidationActualResultAll = "";
 String SuccessActualResult = "";
 String FailureActualResult = "";
 String APINodeConcatLogicType = "";
 String Test = "";
 String dotSymbol = "\\.";
 String hyphenSymbol = "-";
 JSONObject webserviceApiResult;
 SoftAssert s_assert;
 String DuitpintarProviderId = "";
 String DuitpintaremploymentId = "";
 String dphomeloantenor = "";
 String dphomeloanratetype = "";
 String arrivaldate = "";
 String depaturedate = "";
 String dphomeloanpropertystatus = "";

 //String channelCountry ="id";
 String channelCountry = " ";
 String pageUrl = "";
 String ActualResult = "";
 String Duitpintarloan_purposeId = "";
 String DuitpintarcollateralId = "";

 GetExcelInput excelInputInstance = null;
 HashMap < String, HashMap < String, String >> apiNoeTypeAndMapValue = null;

 /*
  * Web Element Configure Values
  */
 String ParentCalssForMoneySmarSite = "//*[1][@class='filters-table-contents']";
 String ChildOneCalssForParentCalss = "filters-table-content";


 @Test
 public void DoFilterAPICall(ITestContext testContext) {

  System.out.println("temp2");
  String testMethodName = "DoFilterAPICall";
  String testMethodNameReport = "Get result from API";
  try {
   System.out.println("temp2");
   // Here we need to get the first set of filter input value to call API. In zero position, there will be the headers of each filters
   calculationMethodTestRunCount++;
   System.out.println("Calculation count = " + calculationMethodTestRunCount);
   // Init the GetExcelInput object. Use this.
   initExcelInputInstacne();
   // Get and set the each filter values in Website.
   getWebDriver().navigate().refresh();
   Thread.sleep(10000);
   /* ExcelInputData excelInputData = ExcelInputData.getInstance();
      GetExcelInput getInput = new GetExcelInput();*/
   String channelName = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
   String pageUrl = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
   channelCountry = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_Country);
   
   
   Thread.sleep(10000);
   String Browser_Firefox = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Firefox);
   String Browser_Chrome = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Chrome);
   String Browser_Safari = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Browser_MethodNameKey, EXCEL_METHODS_INPUT.Browser_Safari);
   //////
   
   getWebDriver().navigate().refresh();
   //getWebDriver().get(pageUrl);
   //getWebDriver().manage().window().maximize();
   Thread.sleep(8000);
     //int year = Calendar.getInstance().get(Calendar.YEAR);
   System.out.println("\n pageUrl = " + pageUrl); 
   System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
   Thread.sleep(8000);
   String token =getWebDriver().getPageSource();
  
   String exacttoke= token.replaceAll("\"", "Q");
   String exacttoken[] = exacttoke.split("\\r\\n|'|\\r"); 
 
   
   if(Browser_Chrome.contains("Y")){  
	   System.out.println("Test "+ Browser_Chrome);	   
	   System.out.println(exacttoken[1]);
	   Constants.MYSTORE.ACESSTOKEN = exacttoken[1];
	     System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");

    } 
 else if(Browser_Firefox.contains("Y")){
	 System.out.println("Test "+ Browser_Firefox);
	 if(pageUrl.contains("travel")){
		 System.out.println(exacttoken[7]);
		   Constants.MYSTORE.ACESSTOKEN = exacttoken[7];
	 }
	 else{
	System.out.println(exacttoken[5]);
   Constants.MYSTORE.ACESSTOKEN = exacttoken[5];
	 }
   System.out.println("Test"+Constants.MYSTORE.ACESSTOKEN );
   System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
}
else
{
	  System.out.println("Test "+ Browser_Safari);
	   System.out.println(exacttoken[1]);
	   System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
	   Constants.MYSTORE.ACESSTOKEN = exacttoken[1];
	     System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
    	  getWebDriver().navigate().to(pageUrl);
	   Thread.sleep(8000);
}
   
   if (!pageUrl.equalsIgnoreCase("https://www.duitpintar.com/en_sg/home-loan")) {
    if (getWebDriver().findElement(By.xpath("//a[@class='btn advanced-filter-trg']")).isDisplayed()) {
     getWebDriver().findElement(By.xpath("//a[@class='btn advanced-filter-trg']")).click();
    } else {
     System.out.println("Adv button invisible InVisible");
    }
   }

   /* WebElement advfilter = getWebDriver().findElement(By.xpath("//a[@class='btn advanced-filter-trg']"));if(advfilter.isDisplayed()){
			 
			 getWebDriver().findElement(By.xpath("//a[@class='btn advanced-filter-trg']")).click();
				Thread.sleep(3000);
			 }*/

   doGetAndSetEachFilterInputInWebsite(channelName, calculationMethodTestRunCount);
   System.out.println("pageUrl= " + pageUrl);
   System.out.println("channelNamechannelName= " + channelName);
   System.out.println("channelCountry2channelCountry2channelCountry2= " + channelCountry);
   // Click calculation button to display the products for calculated input
   doClickCalculateButton();
   Thread.sleep(30000);

   
   ///
   System.out.println("channelCountry2channelCountry2channelCountry2= " + channelCountry);
     String APIResponse = "";

   if (channelName.equalsIgnoreCase("savings-account") || channelName.contains("Savings") || channelName.contains("savings")) {
	   APIResponse = callAPiAndGetResponseForSavings();
   } else {
    APIResponse = callAPiAndGetResponse(getFiltersHashMap(), calculationMethodTestRunCount);
   }

   if (APIResponse != null && !APIResponse.isEmpty()) {
    setAPIResult(APIResponse);

    ExcelInputData excelInput = ExcelInputData.getInstance();
    JSONObject webResultJson;
    try {
     webResultJson = new JSONObject(APIResponse);
     excelInput.setAPIResultJson((Object) webResultJson);
    } catch (JSONException e1) {
     // TODO Auto-generated catch block
     e1.printStackTrace();
    }

    System.out.println("Response fetched from API");
   } else {
    // Please set a result map here that this method failed to retrieve Response from API
    // And also set in attribute to display it in Report
    System.out.println("Error to fetch result from API");
   }

   // Set Result Map here 
  } catch (Exception e) {
   // TODO Auto-generated catch block

   // Set Result Map here for Exception
   e.printStackTrace();
  }
 }






 /* new count */

 @Test
 public void DoGProductCountValidation(ITestContext testContext) {

  String testMethodNameTestNG = "DoGProductCountValidation";

  System.out.println("Start the test method = " + testMethodNameTestNG);
  HashMap < String, Object > resultMap = new HashMap < String, Object > ();
  GetExcelInput getInput = new GetExcelInput();

  String MethodNameKey = "Product Count Validation";
  String SuccessMessage = "Product Count in API and WEB are Equal";
  String SuccessComments = "Product Count Validated Successfully";
  String FailureMessage = "Product Count in API and WEB are not Equal";
  String FailureComments = "Product Count Validation failed";
  String channelName = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
  String pageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
  String expecedResult = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.ProductCountMethodNameKey, EXCEL_METHODS_INPUT.ExpectedResultKey);
  String description = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.ProductCountMethodNameKey, EXCEL_METHODS_INPUT.TestDescriptionKey);
  String MethodExecuteOrNot = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.ProductCountMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
  String actualResult = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.ProductCountMethodNameKey, EXCEL_METHODS_INPUT.ActualResultKey);
  String executeYesOrNo = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.ProductCountMethodNameKey, TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);

  System.out.println("case= " + executeYesOrNo);
  int apiCount = 0;
  int webcount = 0;
  try {
   s_assert = new SoftAssert();
   System.out.println("executeYesOrNo product count Validation= " + executeYesOrNo);

   if (!executeYesOrNo.equalsIgnoreCase("Y")) {
    throw new SkipException("This method skipped");
   }

   try {
    do {
     getWebDriver().findElement(By.xpath("//button[contains(.,'Show More Results')]")).click();
     Thread.sleep(10000);
    } while (true);

   } catch (Exception e) {
    String parentelement = "//*[1][@class='filters-table-contents']";
    String childelement = "filters-table-content";
    Thread.sleep(10000);
    WebElement webElement = getWebDriver().findElement(By.xpath(parentelement));
    List < WebElement > contentList = webElement.findElements(By.className(childelement));
    for (WebElement singleSectionWebElement: contentList) {
     Thread.sleep(4000);
     String productNameWeb = singleSectionWebElement.findElement(
      By.className("bank-title")).getText();
     webcount = contentList.size();
     JSONObject apiResultJson = getAPIResult();
     String premiumParams = apiResultJson.getString("total");
     apiCount = Integer.parseInt(premiumParams);
    }

   }

   // Checking Area

   boolean isAssertFailed = false;
   if (webcount == apiCount) {
    resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
    resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
    resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
    resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
    resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);

   } else {
    resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
    resultMap.put(TEST_RESULT.R_MESSAGE, FailureMessage);
    resultMap.put(TEST_RESULT.R_COMMENTS, FailureComments);
    resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
    FailureMessage = "The Total product Count in API is " + apiCount + " But the product displaying in Website is " + webcount;
    isAssertFailed = true;


   }

   //  * Common Parameter

   resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
   resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
   resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
   resultMap.put(TEST_RESULT.R_DESRIPTION, description);
   resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, MethodExecuteOrNot);
   resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey, channelName);
   resultMap.put(EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey, pageUrl);
   resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);

   boolean isMethodSuccess = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS);
   String resultMessage = (String) resultMap.get(TEST_RESULT.R_MESSAGE);
   testContext.setAttribute(testMethodNameTestNG, resultMap);

   if (isAssertFailed) {
    Assert.fail(FailureMessage);
   }

  } catch (Exception e) {

   if (resultMap == null) {
    resultMap = new HashMap < String, Object > ();
   }
   resultMap.put(TEST_RESULT.R_METHOD_NAME, MethodNameKey);
   resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
   resultMap.put(TEST_RESULT.R_MESSAGE, TEST_RESULT.R_IS_EXCEPTION);
   resultMap.put(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE, e.toString());
   resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, executeYesOrNo);
   resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, expecedResult);
   resultMap.put(TEST_RESULT.R_DESRIPTION, description);
   resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, actualResult);
   testContext.setAttribute(testMethodNameTestNG, resultMap);
   e.printStackTrace();
   Assert.fail(e.toString());
  }
 }

 /* new count end */

 /*
  * Initialiser Methods
  */
 public void initExcelInputInstacne() throws Exception {
  if (this.excelInputInstance == null) {
   this.excelInputInstance = new GetExcelInput();
  }
 }

 /*
  * Get Input Methods
  */
 private HashMap < String, ArrayList < String >> getFiltersHashMap() throws Exception {
  ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
  ExcelCalculationInputData excelCalculationInputData = excelInputDataObject.getExcelCalculationInputData();
  HashMap < String, ArrayList < String >> calculationResultArray = excelCalculationInputData.getMethodInput();
  return calculationResultArray;
 }

 /*
  * Do Action Methods
  */

 private void doGetAndSetEachFilterInputInWebsite(String channelName, int calculationInput) throws Exception {

  System.out.println("channelName = " + channelName);
  System.out.println("calculationInput = " + calculationInput);
  for (Object key: getFiltersHashMap().keySet()) {
   String keyval = (String) key;
   ArrayList < String > inputValuesArray = getFiltersHashMap().get(keyval);
   String calcInputValue = inputValuesArray.get(calculationInput);
   System.out.println("before channelNamekey = " + key);
   Thread.sleep(4000);
   doFilterActionAndSetInputValueBasedOnFilterType(channelName, getFilterWebElement(keyval), getLocator(keyval), calcInputValue);
   System.out.println("after channelNamekey = " + key);
   Thread.sleep(4000);
  }
 }

 public boolean isNeedToGetValueFromExcelEvenIfNotCutomTemplate(String overrideKey, String currentMethodName) throws Exception {
  boolean isNeedToGetValueFromExcelEvenIfCutomTemplate = false;
  String overrideExcelValue = getValueFromExcel(currentMethodName, overrideKey);
  // if(overrideExcelValue.equals("Y")){
  // isNeedToGetValueFromExcelEvenIfCutomTemplate = true;
  //}

  if (overrideExcelValue != null) {
   if (overrideExcelValue.equals("Y")) {
    isNeedToGetValueFromExcelEvenIfCutomTemplate = true;
   }
  }
  return isNeedToGetValueFromExcelEvenIfCutomTemplate;
 }

 public WebDriver getWebDriver() throws Exception {
  ExcelInputData excelInput = ExcelInputData.getInstance();
  WebDriver webDriver = excelInput.getWebDriver();
  return webDriver;
 }
 private void prepareDriver() throws Exception {

	 GetExcelInput getInput = new GetExcelInput();
     String detailPageProductName = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.DetailPageProduct_MethodNameKey, EXCEL_METHODS_INPUT.DetailPageProductName);
     System.out.println("detailPageProductName " + detailPageProductName);

     System.out.println("prepare driver called");
     Thread.sleep(6000);
     getWebDriver().findElement(By.xpath("//a[contains(.,'" + detailPageProductName + "')]")).click();
     Thread.sleep(8000);
  /**
   * Set Driver commonly
   */

 }


 // Get Excel Value Methods
 public String getValueFromExcel(String methodNameKey, String valueKey) throws Exception {
  String valueFromExcel = this.excelInputInstance.get_A_Value_Using_Key_Of_Filters_Method(methodNameKey, valueKey);
  return valueFromExcel;
 }
 public WebElement getFilterWebElement(String currentFilterMethodName) throws Exception {
  GetWebElement obj = new GetWebElement();
  String Locator = getValueFromExcel(currentFilterMethodName, "Web Element Locator");
  String LocatorName = getValueFromExcel(currentFilterMethodName, "Web Element");
  WebElement filterWebElement = obj.getWebElemntFromWebElemntType(getWebDriver(), Locator, LocatorName);
  return filterWebElement;
 }
 public String getLocator(String currentFilterMethodName) throws Exception {
  String requiredValue = "";
  requiredValue = getValueFromExcel(currentFilterMethodName, "Input Control");
  return requiredValue;
 }
 public String getLocatorName(String currentFilterMethodName) throws Exception {
  String requiredValue = "";
  if (isNeedToGetValueFromExcelEvenIfNotCutomTemplate("Override Yaml Value", currentFilterMethodName)) {
   // Get any values here if you need any values from excel even if it is a not a custom template
   requiredValue = getValueFromExcel(currentFilterMethodName, "Web Element");
  } else {
   // get This value from YAML
   requiredValue = getLocatorNameFromYAML(currentFilterMethodName);
  }
  return requiredValue;
 }
 //from YAML
 public String getLocatorNameFromYAML(String currentMethodName) throws Exception {
  String locatorName = "";
  if (getFilterMapFromYAML(currentMethodName).containsKey("span_class")) {
   locatorName = (String) getTableFieldsMapFromYAML(currentMethodName).get("span_class");
  }
  return locatorName;
 }

 public Map getTableFieldsMapFromYAML(String currentMethodName) throws Exception {

  //from YAML
  /* AML2*/
  Map FilterComponentMap = null;
  ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
  Object YAMLObjectValue = excelInputDataObject.getYAMLData();
  Map objectMap = (Map) YAMLObjectValue;
  Object FilterObject = objectMap.get("table_fields");
  Map FilterMap = (Map) FilterObject;
  if (!FilterMap.containsKey("custom_template")) {
   Object ColumbObject = FilterMap.get("columns");
   Map ColumnMap = (Map) ColumbObject;
   Object FieldMapObject = ColumnMap.get(currentMethodName);
   FilterComponentMap = (Map) FieldMapObject;
  }

  return FilterComponentMap;
 }

 public Map getFilterMapFromYAML(String currentFilterMethodName) throws Exception {
  //from YAML
  /* AML2*/
  Map FilterComponentMap = null;
  ExcelInputData excelInputDataObject = ExcelInputData.getInstance();
  Object YAMLObjectValue = excelInputDataObject.getYAMLData();
  Map objectMap = (Map) YAMLObjectValue;
  Object FilterObject = objectMap.get("filter");
  Map FilterMap = (Map) FilterObject;
  Object ValidationsObject = FilterMap.get("validations");
  Map ValidationsMap = (Map) ValidationsObject;
  Object RulesObject = ValidationsMap.get("rules");
  Map RulesMap = (Map) RulesObject;
  String KEYInYAML = getValueFromExcel(currentFilterMethodName, "Filter Key In YAML");
  Object FilterComponentObject = RulesMap.get(KEYInYAML);
  FilterComponentMap = (Map) FilterComponentObject;
  return FilterComponentMap;
 }

 /*
  * Action Methods
  */

 public void doFilterActionAndSetInputValueBasedOnFilterType(String channelName, WebElement webElement, String filtercomponentType, String calculationInput) throws Exception {


  System.out.println("ccchanell " + channelName);
  System.out.println("action channelCountry " + channelCountry);
  System.out.println("action calculationInput " + calculationInput);
  Robot robot = new Robot();
  if (channelCountry.contains("sg")) {
   System.out.println("action filtercomponentType = " + filtercomponentType);
   if (filtercomponentType.equals("textbox")) {
    webElement.clear();
    Thread.sleep(5000);
    webElement.sendKeys(calculationInput);
   } else if (filtercomponentType.equals("selectbox")) {
    webElement.click();
    Thread.sleep(5000);

    /*getWebDriver().findElement(By.xpath("//div[@id='select2-drop-mask']")).click();
					
    */
    System.out.println("selectbox nos selected");
    System.out.println("dro " + ".//*[contains(text(),'" + calculationInput + "')]");
    Thread.sleep(5000);
    WebElement selectbox = getWebDriver().findElement(By.xpath(".//*[@id='select2-drop']"));
    if (selectbox.isDisplayed()) {
     WebElement selbox = selectbox.findElement(By.xpath(".//*[contains(text(),'" + calculationInput + "')]"));
     selbox.click();
     Thread.sleep(5000);
    }
   }

   if (filtercomponentType.equals("multitextbox")) {
    webElement.clear();
    getWebDriver().findElement(By.xpath("//a[@class='select2-search-choice-close']")).click();
    Thread.sleep(3000);
    webElement.clear();
    robot.keyPress(KeyEvent.VK_ENTER);
    Thread.sleep(5000);
    String allcountries = calculationInput;
    System.out.println("st");
    String splitter[] = allcountries.split(",");
    //int totalcountry = splitter.length;
    for (int i = 0; i < splitter.length; i++) {
     webElement.sendKeys(splitter[i]);
     Thread.sleep(3000);   
     webElement.sendKeys(Keys.ENTER);
     Thread.sleep(3000); 

    }
   
    robot.keyPress(KeyEvent.VK_ENTER);
   } else if (filtercomponentType.equals("calendar")) {
    
    System.out.println("Started the calendar control method");
    System.out.println(webElement);

    Thread.sleep(5000);
   
    robot.keyPress(KeyEvent.VK_PAGE_UP);
    robot.keyPress(KeyEvent.VK_PAGE_UP);
    
    webElement.sendKeys(Keys.PAGE_UP);
    webElement.sendKeys(Keys.PAGE_UP);
    webElement.click();
    
    String startdate = calculationInput;
    String splitter[] = startdate.split("-");
    //String startyear = splitter[2];
    String checkstartmonthandyear = splitter[1];
    String checkstartday = splitter[0];
    String parentWindowHandle = getWebDriver().getWindowHandle();
    Thread.sleep(2000);
  
    selectCalendarone(checkstartmonthandyear, checkstartday);

    Thread.sleep(2000);

    getWebDriver().switchTo().window(parentWindowHandle);
    webElement.sendKeys(Keys.ENTER);
    System.out.println("upto enterclick");
    
    Thread.sleep(3000);
    webElement.sendKeys(Keys.TAB);
    webElement.sendKeys(Keys.ENTER);
    webElement.sendKeys(Keys.ENTER);
     } else if (filtercomponentType.equals("checkbox")) {
    WebElement wb = webElement;


    WebElement chkbox = wb.findElement(By.xpath(".//label[contains(.,'" + calculationInput + "')]"));
    chkbox.click();

    Thread.sleep(5000);

  
   } else if (filtercomponentType.equals("radiobutton")) {

    webElement.click();
    Thread.sleep(3000);
    WebElement rbutton = getWebDriver().findElement(By.xpath(".//label[contains(.,'" + calculationInput + "')]"));
    rbutton.click();
    Thread.sleep(5000);
    }

  } else if (channelCountry.contains("id")) {


   System.out.println("action filtercomponentType = " + filtercomponentType);
   if (filtercomponentType.equals("textbox")) {
    webElement.clear();

    Thread.sleep(5000);
    webElement.sendKeys(calculationInput);
   } else if (filtercomponentType.equals("selectbox")) {

    System.out.println(channelName);
    System.out.println("select box section");
    webElement.click();
    Thread.sleep(5000);
    String urlHomeLoan = "https://www.duitpintar.com/en_sg/home-loan";
    String urlDPHomeLoan = "https://www.duitpintar.com/kredit-pemilikan-rumah";
    if (channelName.equals("multi-purpose-loan")) {

     //***********************************//
     if (pageUrl.contains("kredit-multiguna")) {
      WebElement iam = getWebDriver().findElement(By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
      iam.click();
      Thread.sleep(5000);
      Duitpintarloan_purposeId = getWebDriver().findElement(By.id("loan_purpose")).getAttribute("value");
      System.out.println("Duitpintarloan_purposeId= " + Duitpintarloan_purposeId);
      Thread.sleep(5000);
      DuitpintarcollateralId = getWebDriver().findElement(By.id("collateral_type")).getAttribute("value");
      System.out.println("DuitpintarcollateralId= " + DuitpintarcollateralId);
     } else {
      System.out.println("12312312312313 " + channelName);
      WebElement iam = getWebDriver().findElement(By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
      iam.click();
      Thread.sleep(5000);
     }
    } else if (channelName.equals("Home Loan")) {

     System.out.println("dp hoam loan ");

     if (pageUrl.equals(urlHomeLoan)) {
      WebElement iam = getWebDriver().findElement(By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
      iam.click();
      Thread.sleep(5000);
      //***********************************//
      dphomeloantenor = getWebDriver().findElement(By.id("tenors")).getAttribute("value");
      System.out.println("dphomeloantenor= " + dphomeloantenor);


      dphomeloanratetype = getWebDriver().findElement(By.id("interest-type")).getAttribute("value");
      System.out.println("dphomeloanratetype= " + dphomeloanratetype);

      dphomeloanpropertystatus = getWebDriver().findElement(By.id("property-status")).getAttribute("value");
      System.out.println("dphomeloanpropertystatus= " + dphomeloanpropertystatus);



     } else if (pageUrl.equals(urlDPHomeLoan)) {

      WebElement iam = getWebDriver().findElement(By.xpath(".//*[contains(text(), '" + calculationInput + "')]"));
      iam.click();
      Thread.sleep(5000);

      //***********************************//
      dphomeloantenor = getWebDriver().findElement(By.id("tenors")).getAttribute("value");
      System.out.println("dphomeloantenor= " + dphomeloantenor);
      
      dphomeloanratetype = getWebDriver().findElement(By.id("interest-type")).getAttribute("value");
      System.out.println("dphomeloanratetype= " + dphomeloanratetype);

      dphomeloanpropertystatus = getWebDriver().findElement(By.id("property-status")).getAttribute("value");
      System.out.println("dphomeloanpropertystatus= " + dphomeloanpropertystatus);

     }


    }


   }

  if (filtercomponentType.equals("multitextbox")) {
    webElement.clear();
    robot.keyPress(KeyEvent.VK_ENTER);
    Thread.sleep(5000);
    
    String allcountries = calculationInput;
    System.out.println("st");
    String splitter[] = allcountries.split(",");
    //int totalcountry = splitter.length;
    for (int i = 0; i < splitter.length; i++) {
     webElement.sendKeys(splitter[i]);
     Thread.sleep(3000);
   
     webElement.sendKeys(Keys.ENTER);
     Thread.sleep(3000);
     

    }
   
    robot.keyPress(KeyEvent.VK_ENTER);
   } else if (filtercomponentType.equals("calendar")) {
   
    System.out.println("Started the calendar control method");
    System.out.println(webElement);

    Thread.sleep(5000);
  
    robot.keyPress(KeyEvent.VK_PAGE_UP);
    robot.keyPress(KeyEvent.VK_PAGE_UP);
  
    webElement.sendKeys(Keys.PAGE_UP);
    webElement.sendKeys(Keys.PAGE_UP);
    webElement.click();
   
    String startdate = calculationInput;
    String splitter[] = startdate.split("-");
    //String startyear = splitter[2];
    String checkstartmonthandyear = splitter[1];
    String checkstartday = splitter[0];
    String parentWindowHandle = getWebDriver().getWindowHandle();
    Thread.sleep(2000);
   
    selectCalendarone(checkstartmonthandyear, checkstartday);

    Thread.sleep(2000);

    getWebDriver().switchTo().window(parentWindowHandle);
    webElement.sendKeys(Keys.ENTER);
    System.out.println("upto enterclick");
   
    Thread.sleep(3000);
    webElement.sendKeys(Keys.TAB);
    webElement.sendKeys(Keys.ENTER);
    webElement.sendKeys(Keys.ENTER);
    
   } else if (filtercomponentType.equals("checkbox")) {

    WebElement wb = webElement;
    WebElement chkbox = wb.findElement(By.xpath(".//label[contains(.,'" + calculationInput + "')]"));
    chkbox.click();
    Thread.sleep(5000);;
   } else if (filtercomponentType.equals("radiobutton")) {
    webElement.click();
    //webElement.click();
    Thread.sleep(3000);
    WebElement rbutton = getWebDriver().findElement(By.xpath(".//label[contains(.,'" + calculationInput + "')]"));
    rbutton.click();
    Thread.sleep(5000);
   } else {
    System.out.println("= ");
   }
  }
 }

 /*
  * Other Helper Methods
  */

 public void doClickCalculateButton() throws Exception {
  Thread.sleep(4000);
  getWebDriver().findElement(By.id(locatorNameForNLFFILtercalculateButton)).click();
  Thread.sleep(8000);
  // Check whether calculation button is allowed or not and return a boolean
 }

 /*
  * Do Helper Methods
  */

 private String callAPiAndGetResponse(HashMap < String, ArrayList < String >> filtersKeyValue, int apiInvocationCount) throws Exception {

  System.out.println("Call API and get response Method");
  String apiRequestUrl = createWebserviceRequestUrl(filtersKeyValue, apiInvocationCount);
  WebserviceRequest webserviceRequest = new WebserviceRequest();
  webserviceListener = this;
  System.out.println("Webservice Request : " + apiRequestUrl);
  String jsonResult = webserviceRequest.GET(webserviceListener, apiRequestUrl);
  if (jsonResult.isEmpty() || jsonResult == null) {
   System.out.println("Result is empty or null");
  }
  return jsonResult;
 }
 /*
  * Product Name Validation Start
  */
 @Test
 public void DoProductValidation(ITestContext testContext) {

  String testMethodName = "DoProductValidation";
  String testMethodNameReport = "Product List Validation";

  String productValidationMethodNameKey = "DoProductValidation";
  HashMap < String, Object > resultMap = new HashMap < String, Object > ();
  try {

   System.out.println("Filter Validation Count = " + productValidationMethodInvocationCount);

   productValidationMethodInvocationCount++;

   int getRespectiveProductValidationCountFromExcel = productValidationMethodInvocationCount - 1;

   System.out.println("getRespectiveProductValidationCountFromExcel = " + getRespectiveProductValidationCountFromExcel);

   String CountryType = "";
   String PageUrl = "";
   String executeOrNotExcel = "";
   String Locator = "";
   String LocatorName = "";
   String APINodeExcel = "";
   String productValidationDescription = "";
   String productValidationExpectedResult = "";
   String productValidationActualResult = "";
   String APINodeExcelSecond = "";
   /*
    * Get required input values from Excel. Filter section Get Values start
    */
   ExcelInputData excelInputData = ExcelInputData.getInstance();
   GetExcelInput getInput = new GetExcelInput();

   String channelName = this.excelInputInstance.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
   System.out.println("channelNamechannelNamechannelName = " + channelName);
   /* AML2*/
   ArrayList < HashMap < String, ExcelProductValidationInputData >> excelInputProductValidatinArray = excelInputData.getExcelProductValidationInputData();
   HashMap < String, ExcelProductValidationInputData > excelInputProductValidationrMap = null;
   excelInputProductValidationrMap = excelInputProductValidatinArray.get(getRespectiveProductValidationCountFromExcel);

    productValidationMethodNameKey = (String) excelInputProductValidationrMap.keySet().toArray()[getRespectiveProductValidationCountFromExcel];
   System.out.println("\n =====================" + productValidationMethodNameKey + "=====================");


   executeOrNotExcel = getInput.get_A_Value_Using_Key_Of_A_Method(productValidationMethodNameKey.toString().trim(), TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
   System.out.println("executeOrNotExcel all method = " + executeOrNotExcel);
   executeOrNotExcel = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey.toString().trim(), TEST_RESULT.R_METHOD_EXECUTE_OR_NOT);
   System.out.println("executeOrNotExcel pro method = " + executeOrNotExcel);


   System.out.println(" executeOrNotExcel : " + executeOrNotExcel);

   if (executeOrNotExcel != null && !executeOrNotExcel.isEmpty() && !executeOrNotExcel.equalsIgnoreCase("Y")) {
    throw new SkipException("Product validation skippped for Filter: " + productValidationMethodNameKey);
   }

   /* AML2*/
   ExcelProductValidationInputData productValidation_InputData = excelInputProductValidationrMap.get(productValidationMethodNameKey);
   HashMap < String, String > productValidationInputDataMap = productValidation_InputData.getMethodInput();
    CountryType = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, "Country");
   PageUrl = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChanellURlKey);
   Locator = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Web Element Locator");
   LocatorName = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Web Element");
   APINodeExcel = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Api Node");
   ActualResult = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Actual Result");
   System.out.println("APINodeExcel Prod : " + APINodeExcel);

   if (APINodeExcel.contains("|")) {
    APINodeExcelSecond = APINodeExcel;
    String rep = APINodeExcel.replace("|", hyphenSymbol);
    String trimVal = rep.split(hyphenSymbol)[1];
    System.out.println("trimVal: " + trimVal);

    APINodeExcel = trimVal.toString().trim();
   }
  if (APINodeExcel.contains("raw_features")) {
    APINodeExcel = APINodeExcel.replace("raw_features", "features").toString().trim();
   }
   productValidationDescription = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Test Description");
   productValidationExpectedResult = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Expected Test Result");
   productValidationActualResult = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Actual Result");

   if (productValidationDescriptionAll.length() == 0) {
    productValidationDescriptionAll = productValidationDescriptionAll + productValidationDescription;
   } else {
    productValidationDescriptionAll = productValidationDescriptionAll + ", " + productValidationDescription;
   }

   if (productValidationExpectedResultAll.length() == 0) {
    productValidationExpectedResultAll = productValidationExpectedResultAll + productValidationExpectedResult;
   } else {
    productValidationExpectedResultAll = productValidationExpectedResultAll + ", " + productValidationExpectedResult;
   }

   if (productValidationActualResultAll.length() == 0) {
    productValidationActualResultAll = productValidationActualResultAll + productValidationActualResult;
   } else {
    productValidationActualResultAll = productValidationActualResultAll + ", " + productValidationActualResult;
   }

   String staticLocator = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Bank Name", "Web Element Locator");
   String staticElement = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Bank Name", "Web Element");

   System.out.println(".. staticLocator = " + staticLocator);
   System.out.println(".. Locator = " + Locator);
   System.out.println(".. LocatorName = " + LocatorName);
   System.out.println(".. APINodeExcel = " + APINodeExcel);
   Locator = "className";
   System.out.println(".. staticElement = " + staticElement);
   System.out.println(".. staticLocator = " + Locator);

   /*
    * Read YAML Input 
    */
   /* AML2*/
   ExcelInputData excelInputDataObject = ExcelInputData.getInstance();

   /*
    *  Check Condition
    */

   // Get the no of input counts from Excel
   ExcelInputData excelInput = ExcelInputData.getInstance();
   ExcelCalculationInputData excelCalculationInputData = excelInput.getExcelCalculationInputData();
   HashMap < String, ArrayList < String >> calculationResultArray = excelCalculationInputData.getMethodInput();
   Set < String > calculationResultKeySet = calculationResultArray.keySet();

   int filterInputValueCount = 0;
   for (String filterKey: calculationResultKeySet) {
    filterInputValueCount = calculationResultArray.get(filterKey).size();
    break;
   }

   System.out.println("Input count = " + filterInputValueCount);

   WebElement element = getWebDriver().findElement(By
    .xpath(ParentCalssForMoneySmarSite));
   List < WebElement > contentList = element.findElements(By
    .className(ChildOneCalssForParentCalss));

   Thread.sleep(10000);
   if (productValidationMethodNameKey.equals("Bank Name")) {

    String needTrim = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Trim");
    String trimValue = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Trim Value");

    String isConcat = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Concat");
    String concatType = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Concat Type");
    String concatValue = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey, "Concat Value");

    System.out.println("Locator = " + Locator);
    System.out.println("LocatorName = " + LocatorName);
    System.out.println("APINodeExcel = " + APINodeExcel);
    System.out.println("needTrim = " + needTrim);
    System.out.println("trimValue = " + trimValue);
    System.out.println("isConcat = " + isConcat);
    System.out.println("concatType = " + concatType);
    System.out.println("concatValue = " + concatValue);

    boolean isAnotherApiNode = false;
    boolean isNeedTrim = false;
    boolean isNeedisConcat = false;

    String productTypeAnotherAPINode = "";

    if (needTrim.contains("Y")) {
     isNeedTrim = true;
    }

    if (isConcat.contains("Y")) {
     isNeedisConcat = true;
    }

    // Get the array from Web 
    List < String > checkingValueArrayFromWeb = getListOfProductValueFromWebForParticularTypeInArray(contentList, Locator, LocatorName);

    for (int webValueCoutn = 0; webValueCoutn < checkingValueArrayFromWeb.size(); webValueCoutn++) {
     System.out.println("Web Value Name = " + checkingValueArrayFromWeb.get(webValueCoutn));
    }

    List < String > checkingValueArrayFromApi = getListOfProductValueFromAPIForParticularTypeInArray(
     getAPIResult(), APINodeExcel,
     isAnotherApiNode, productTypeAnotherAPINode, dotSymbol, CountryType,
     true, isNeedTrim, trimValue,
     isNeedisConcat, concatType, concatValue);

    for (int apiValueCoutn = 0; apiValueCoutn < checkingValueArrayFromApi.size(); apiValueCoutn++) {
     System.out.println("API Value Name = " + checkingValueArrayFromApi.get(apiValueCoutn));
    }

    
    /*
     * Check bank name
     */
    
    for (int productCount = 0; productCount < checkingValueArrayFromApi.size(); productCount++) {
     String productInWebAndAPI = "";

     String productInWeb = checkingValueArrayFromWeb.get(productCount);
     String productInApi = checkingValueArrayFromApi.get(productCount);
     System.out.println("Product Validation   xxxxxxxxx productInApi:" + productInApi);
     System.out.println("Product Validation   xxxxxxxxx productInWeb:" + productInWeb);


     if (productInWeb.contains("Starting")) {
      productInWeb = productInWeb.split("Starting")[0].trim();
     }

     if (productInWeb.contains("Rate Type")) {
      productInWeb = productInWeb.replace("Rate Type", "").trim();
     }

     if (productInWeb.contains("Lock-in Period")) {
      productInWeb = productInWeb.replace("Lock-in Period", "").trim();
     }

     if (productInWeb.contains("Interest Rate")) {
      productInWeb = productInWeb.replace("Interest Rate", "").trim();
     }
     if (productInWeb.contains("(1 year Average)")) {
      productInWeb = productInWeb.replace("(1 year Average)", "").trim();
     }
     if (productInWeb.contains("Monthly Instalments")) {
      productInWeb = productInWeb.replace("Monthly Instalments", "").trim();
      System.out.println("productInWeb123: " + productInWeb);
     }
     if (productInWeb.contains("No Lock-in")) {
      productInWeb = productInWeb.replace("No Lock-in", "0").trim();

     }
     if (productInWeb.contains("%")) {
      productInWeb = productInWeb.replace("%", "").trim();
     }
     if (productInWeb.contains("$")) {
      productInWeb = productInWeb.replace("$", "").trim();
     }
     if (productInWeb.contains(",")) {
      productInWeb = productInWeb.replace(",", "").trim();
     }

   
     if (productInWeb.contains("Trip Cancellation")) {
      productInWeb = productInWeb.replace("Trip Cancellation", "").trim();
     }

     if (productInWeb.contains("Loss/Damage of Baggage")) {
      productInWeb = productInWeb.replace("Loss/Damage of Baggage", "").trim();
     }

     if (productInWeb.contains("Asia")) {
      productInWeb = (productInWeb.split("Asia")[0]).trim();
    
     }
     if (productInWeb.contains("ASEAN")) {
      productInWeb = (productInWeb.split("ASEAN")[0]).trim();
     
     }
     if (productInWeb.contains("Zone")) {
      productInWeb = (productInWeb.split("Zone")[0]).trim();
  
     }
     if (productInWeb.contains("South-East Asia")) {
      productInWeb = (productInWeb.split("Zone")[0]).trim();
     
     }

     if (productInWeb.contains("Global")) {
      productInWeb = (productInWeb.split("Global")[0]).trim();
   
     }

     if (productInWeb.contains("Worldwide")) {
      productInWeb = (productInWeb.split("Worldwide")[0]).trim();
    
     }

     if (productInApi.contains("Rp")) {
      String replace_Value = productInWeb.replace("Rp", hyphenSymbol);
      System.out.println("replaceValue: " + replace_Value);
      productInApi = replace_Value.split(hyphenSymbol)[1];

      System.out.println("productInApi Rp : " + productInApi);
     }

     
     System.out.println("productInWeb check: " + productInWeb);
     System.out.println("productInApi check: " + productInApi);

     //if(channelName.contains("Fixed Deposit")){

     if (APINodeExcelSecond.contains("|")) {
      String rep = APINodeExcelSecond.replace("|", hyphenSymbol);
      String trimVal = rep.split(hyphenSymbol)[0];
      System.out.println("LOGGER: " + trimVal);
      String apiVal = getAnotherAPIValue(getAPIResult(), trimVal, dotSymbol, CountryType, productCount);
      System.out.println("apiVal: " + apiVal);

      productInApi = apiVal.toString().trim() + " " + productInApi;

      System.out.println("conact api value: " + productInApi);
     }
     //}

     if (productInWeb.toString().trim().equals(productInApi.toString().trim())) {

      System.out.println("Product Validation : Passed");

      System.out.println("productInWeb : " + productInWeb);
      System.out.println("productInApi : " + productInApi);

      SuccessActualResult = ActualResult;

      //SuccessActualResult  = "There are "+calculationMethodTestRunCount+" calculation records in the excel. So this method excuted "+calculationMethodTestRunCount+"times"+"\n";

     } else {
      isAllProductNameValidatedAndSucess = false;
      System.out.println("Product Validation : Failed");
      System.out.println("productInWeb : " + productInWeb);
      System.out.println("productInApi : " + productInApi);
      productInWebAndAPI = productInWeb + ", " + productInApi;
      System.out.println("Product Validation  productNameWebAndAPI:" + productInWebAndAPI);

      FailureActualResult = FailureActualResult + "(WeB: " + checkingValueArrayFromWeb + " *** API: " + checkingValueArrayFromApi + ") This product Count is not matching with API product Count," + " \n";
     }
     /*
      * Checking Area
      */
     if (isAllProductNameValidatedAndSucess) {
      resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
      resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
      resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
      resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, SuccessActualResult);
     } else {
      System.out.println("Product Validation failed" + FailureActualResult);
      resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
      resultMap.put(TEST_RESULT.R_MESSAGE, FailureActualResult);
      resultMap.put(TEST_RESULT.R_COMMENTS, FailureActualResult);
      resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, FailureActualResult);
      Assert.fail(FailureActualResult);
     }
    }
   } else {

    //String LogicType = "normal";
    boolean isAnotherApiNode = false;

    String productTypeAPINode = APINodeExcel;
    String productTypeAnotherAPINode = APINodeExcel;

    ///add excel here

    if (!productValidationMethodNameKey.equals("Bank Name")) {
     concatIfAnyStringsNeedToConcatInYAML(productTypeAnotherAPINode, CountryType);
     HashMap < String, String > resultMapAPI = getAPINodeValueToParse(productTypeAnotherAPINode, CountryType);
     productTypeAPINode = resultMapAPI.get("apiNodeValue");
     if (resultMapAPI.get("isAnotherAPIConcat").contains("Y")) {
      isAnotherApiNode = true;
      productTypeAnotherAPINode = resultMapAPI.get("apiValueTobeConcat");
     }
    }

    // Get the array from Web 
    List < String > checkingValueArrayFromWeb = getListOfProductValueFromWebForParticularTypeInArray(contentList, Locator, LocatorName);

    for (int webValueCoutn = 0; webValueCoutn < checkingValueArrayFromWeb.size(); webValueCoutn++) {
     System.out.println("Web Value  = " + checkingValueArrayFromWeb.get(webValueCoutn));
    }
    // Get the array from API Result

    if (getAPIResult() == null || productTypeAPINode == null) {
     System.out.println("Json/productTypeAPINode has null value : " + productTypeAPINode);
    }

    List < String > checkingValueArrayFromApi = getListOfProductValueFromAPIForParticularTypeInArray(
     getAPIResult(), productTypeAPINode,
     isAnotherApiNode, productTypeAnotherAPINode, dotSymbol, CountryType,
     false, false, "",
     false, "", "");

    for (int apiValueCoutn = 0; apiValueCoutn < checkingValueArrayFromApi.size(); apiValueCoutn++) {
     System.out.println("API Value  = " + checkingValueArrayFromApi.get(apiValueCoutn));
    }

    int productcountinweb = 0;

    WebElement showall;
    try {
     showall = getWebDriver().findElement(By.id("total-info"));

     if (showall.isDisplayed()) {

      String totalProductCount = getWebDriver().findElement(By.id("total-info")).getText();
      Thread.sleep(9000);
      String[] productvalues = totalProductCount.split("products");
      String totalproductvalue = productvalues[0];
      productcountinweb = Integer.parseInt(totalproductvalue.trim());
     } else {
      System.out.println("not display toyal info:");
     }
    } catch (Exception e) {
     showall = getWebDriver().findElement(By.className("all-results-loaded"));

     if (showall.isDisplayed()) {

      WebElement elementTotalProd = null;
      By elementIdentifiedBy = By.className("all-results-loaded");

      //filters-result-info product-results-count
      Thread.sleep(5000);

      elementTotalProd = getWebDriver().findElement(elementIdentifiedBy);


      Thread.sleep(3000);
      String allResultsShowed = "";
      if (elementTotalProd != null) {
       allResultsShowed = elementTotalProd.getText().toString().trim();
       System.out.println("allResultsShowed not null :" + allResultsShowed);
      }


      if (allResultsShowed.contains("Showing")) {
       String replaceVal = allResultsShowed.replace("Showing", hyphenSymbol);
       String firstVal = replaceVal.split(hyphenSymbol)[1];

       System.out.println("firstVal :" + firstVal);

       String replaceSpace = firstVal.replace("matching", hyphenSymbol);
       String totalproductvalue = replaceSpace.split(hyphenSymbol)[0];

       System.out.println("totalproductvalue :" + totalproductvalue);

       productcountinweb = Integer.parseInt(totalproductvalue.toString().trim());
      } else if (allResultsShowed.contains("Menampilkan")) {
       String replaceVal = allResultsShowed.replace("Menampilkan", hyphenSymbol);
       String firstVal = replaceVal.split(hyphenSymbol)[1];

       System.out.println("MenampilkanfirstVal :" + firstVal);

       String replaceSpace = firstVal.replace("sesuai", hyphenSymbol);
       String totalproductvalue = replaceSpace.split(hyphenSymbol)[0];

       System.out.println("sesuaitotalproductvalue :" + totalproductvalue);

       productcountinweb = Integer.parseInt(totalproductvalue.toString().trim());
      }
     } else {
      System.out.println("not display all reslt info:");
     }
    }


    for (int productCount = 0; productCount < checkingValueArrayFromWeb.size(); productCount++) {
     String productInWebAndAPI = "";

     String productInWeb = checkingValueArrayFromWeb.get(productCount);
     String productInApi = checkingValueArrayFromApi.get(productCount);

     if (productValidationMethodNameKey.equals("Price")) {


      double value = Double.parseDouble(productInApi);
      double finalValue = Math.round(value);
      productInApi = Double.toString(finalValue);

      double obj = new Double(productInApi);

      int intValue = (int) obj;
      productInApi = Integer.toString(intValue);

      System.out.println("Product Validation inside doub:" + productInApi);

     }


     System.out.println("Product Validation   %%%%%%% productInApi:" + productInApi);
     System.out.println("Product Validation   %%%%%%% productInWeb:" + productInWeb);

     if (productInApi.contains("Rp.")) {
      String replace_Value = productInApi.replace("Rp.", hyphenSymbol);
      System.out.println("replaceValue: " + replace_Value);
      productInApi = replace_Value.split(hyphenSymbol)[1];

      System.out.println("productInApi Rp : " + productInApi);
     }


     if (productInWeb.contains("bulan")) {
      String replaceValueAnnually = productInWeb.replace("bulan", hyphenSymbol);
      System.out.println("replaceValue bulan: " + replaceValueAnnually);

      productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
      System.out.println("productInWeb bulan: " + productInWeb);

     }

     if (productInWeb.contains("Tenor Pinjaman")) {
      String replaceValueAnnually = productInWeb.replace("Tenor Pinjaman", hyphenSymbol);
      System.out.println("replaceValue Tenor Pinjaman: " + replaceValueAnnually);

      productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
      System.out.println("productInWebTenor Pinjaman: " + productInWeb);
     }

     if (productInWeb.contains("Tiap Tahun")) {
      String replaceValueAnnually = productInWeb.replace("Tiap Tahun", hyphenSymbol);
      System.out.println("replaceValue Tiap Tahun: " + replaceValueAnnually);

      productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
      System.out.println("productInWebTiap Tahun: " + productInWeb);

     }

     if (productInWeb.contains("Tiap Bulan")) {
      String replaceValueAnnually = productInWeb.replace("Tiap Bulan", hyphenSymbol);
      System.out.println("replaceValue Tiap Tahun: " + replaceValueAnnually);

      productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
      System.out.println("productInWebTiap Bulan: " + productInWeb);

     }

     //	String[] Countryarray = countryValue.split(",");
     if (productInWeb.contains(",")) {
      String replaceValueComa = productInWeb.replace(",", "");
      System.out.println("replaceValue Coma : " + replaceValueComa);

      productInWeb = replaceValueComa;
      System.out.println("productInWeb Coma: " + productInWeb);

     }


     if (productInWeb.contains("Rp.")) {
      String replace_Value = productInWeb.replace("Rp.", hyphenSymbol);
      System.out.println("replaceValue: " + replace_Value);
      productInWeb = replace_Value.split(hyphenSymbol)[1];

      System.out.println("productInWeb Rp : " + productInWeb);
     }


     /*
      * Checking Area
      */

     if (productInWeb.contains("months")) {
      String replaceValueAnnually = productInWeb.replace("months", hyphenSymbol);
      System.out.println("replaceValue months: " + replaceValueAnnually);

      productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
      System.out.println("productInWeb months: " + productInWeb);

     }

     if (productInWeb.contains("Annually")) {
      String replaceValueAnnually = productInWeb.replace("Annually", hyphenSymbol);
      System.out.println("replaceValue Annually " + replaceValueAnnually);

      productInWeb = replaceValueAnnually.split(hyphenSymbol)[0];
      System.out.println("productInWeb trimmed Annually: " + productInWeb);

     }


     if (productInWeb.contains("Per Month")) {
      String replaceValue = productInWeb.replace("Per Month", hyphenSymbol);
      System.out.println("replaceValue per month: " + replaceValue);

      productInWeb = replaceValue.split(hyphenSymbol)[0];
      System.out.println("productInWeb trimmed per month: " + productInWeb);

      productInWeb = productInWeb.replace(",", "");
      System.out.println(" After Trim replaceValuemonth in Web: " + productInWeb);
      // productInWeb = replaceValue.split(hyphenSymbol)[0];

      if (productInWeb.contains("IDR")) {
       String _replaceValue = productInWeb.replace("IDR", hyphenSymbol);
       System.out.println("replaceValue: " + _replaceValue);
       productInWeb = _replaceValue.split(hyphenSymbol)[1];

       System.out.println("productInWeb IDR : " + productInWeb);
      }

     } else if (productInWeb.contains("Interest Earned")) {
      String replaceValue = productInWeb.replace("Interest Earned", hyphenSymbol);
      System.out.println("replaceValue: " + replaceValue);
      productInWeb = replaceValue.split(hyphenSymbol)[0];

      System.out.println("Interest Earned trimmed : " + productInWeb);
     } else if (productInWeb.contains("Annual Interest Rate")) {
      String replaceValue = productInWeb.replace("Annual Interest Rate", hyphenSymbol);
      System.out.println("replaceValue: " + replaceValue);
      productInWeb = replaceValue.split(hyphenSymbol)[0];

      System.out.println("Annual Interest Rate trimmed : " + productInWeb);
     } else if (productInWeb.contains("Interest Rate")) {
      String replaceValue = productInWeb.replace("Interest Rate", hyphenSymbol);
      System.out.println("replaceValue: " + replaceValue);
      productInWeb = replaceValue.split(hyphenSymbol)[0];

      System.out.println("productInWeb trimmed : " + productInWeb);
     }
     ///////////////////
     else if (productInWeb.contains("Loan")) {
      if (productInWeb.contains("Tenure")) {
       String replaceValue = productInWeb.replace("Loan Tenure", hyphenSymbol);
       System.out.println("replaceValue tenure: " + replaceValue);
       productInWeb = replaceValue.split(hyphenSymbol)[0];

       System.out.println("productInWeb trimmed loan: " + productInWeb);
      } else {
       String replaceValue = productInWeb.replace("Loan", hyphenSymbol);
       System.out.println("replaceValue: " + replaceValue);
       productInWeb = replaceValue.split(hyphenSymbol)[0];

       System.out.println("productInWeb trimmed loan: " + productInWeb);
      }
      String replaceValue = productInWeb.replace("Loan Tenure", hyphenSymbol);
      System.out.println("replaceValue: " + replaceValue);
      productInWeb = replaceValue.split(hyphenSymbol)[0];

      System.out.println("productInWeb trimmed : " + productInWeb);
     } else if (productInWeb.contains("Tenure")) {
      if (productInWeb.contains("Loan Tenure")) {

      } else {
       String replaceValue = productInWeb.replace("Tenure", hyphenSymbol);
       System.out.println("replaceValue: " + replaceValue);
       productInWeb = replaceValue.split(hyphenSymbol)[0];

       System.out.println("productInWeb Tenure : " + productInWeb);
      }

     } else if (productInWeb.contains("Monthly rest")) {
      String replaceValue = productInWeb.replace("Monthly rest", hyphenSymbol);
      System.out.println("replaceValue: " + replaceValue);
      productInWeb = replaceValue.split(hyphenSymbol)[0];

      System.out.println("productInWeb trimmed : " + productInWeb);
     } else if (productInWeb.contains("Flat rate")) {
      String replaceValue = productInWeb.replace("Flat rate", hyphenSymbol);
      System.out.println("replaceValue: " + replaceValue);
      productInWeb = replaceValue.split(hyphenSymbol)[0];

      System.out.println("productInWeb trimmed : " + productInWeb);
     }





     if (productInWeb.contains("$")) {
      productInWeb = productInWeb.replace("$", "").trim();
     }
     if (productInWeb.contains(",")) {
      productInWeb = productInWeb.replace(",", "").trim();
     }

     if (productInWeb.contains("Medical")) {

      productInWeb = (productInWeb.split("Medical")[0]).trim();

      System.out.println("productInWeb  : Medical " + productInWeb);
      
     }

     if (productInWeb.contains("Trip Cancellation")) {
      productInWeb = productInWeb.replace("Trip Cancellation", "").trim();
     }

     if (productInWeb.contains("Loss/Damage of Baggage")) {
      productInWeb = productInWeb.replace("Loss/Damage of Baggage", "").trim();
     }

     if (productInWeb.contains("Trip Cancellation")) {
      productInWeb = productInWeb.replace("Trip Cancellation", "").trim();
     }

     if (productInWeb.contains("per family")) {
      productInWeb = (productInWeb.split("per family")[0]).trim();
   
     }

     if (productInWeb.contains("per adult")) {
      productInWeb = (productInWeb.split("per adult")[0]).trim();
  
     }


     if (productInWeb.contains("Loss/Damage of Baggage")) {
      productInWeb = (productInWeb.split("Loss/Damage of Baggage")[0]).trim();
   
     }

     System.out.println("productInWeb checker: " + productInWeb);
     System.out.println("productInApi checker: " + productInApi);

     if (productInWeb.toString().trim().equalsIgnoreCase(productInApi.toString().trim())) {


      //if (productInWeb.toString().trim().equals(productInApi.toString().trim())) {
      System.out.println("Product Validation : Passed");

      SuccessActualResult = ActualResult;
      //SuccessActualResult  = "There are "+calculationMethodTestRunCount+" calculation records in the excel. So this method excuted "+calculationMethodTestRunCount+"times"+"\n";

      //} 
     } else {
      isAllProductNameValidatedAndSucess = false;
      System.out.println("Product Validation : Failed");
      System.out.println("productInWeb : " + productInWeb);
      System.out.println("productInApi : " + productInApi);
      productInWebAndAPI = productInWeb + ", " + productInApi;
      System.out.println("Product Validation  else case:" + productInWebAndAPI);

      FailureActualResult = FailureActualResult + "(WeB: " + checkingValueArrayFromWeb + " *** API: " + checkingValueArrayFromApi + ") This product Count is not matching with API product Count," + " \n";
     }

     /*
      * Checking Area
      */
     if (isAllProductNameValidatedAndSucess) {
      resultMap.put(TEST_RESULT.R_IS_SUCCESS, true);
      resultMap.put(TEST_RESULT.R_MESSAGE, SuccessMessage);
      resultMap.put(TEST_RESULT.R_COMMENTS, SuccessComments);
      resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, SuccessActualResult);
     } else {

      ///////////////////////////////
      System.out.println("Product Validation failed" + FailureActualResult);
      resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
      resultMap.put(TEST_RESULT.R_MESSAGE, FailureActualResult);
      resultMap.put(TEST_RESULT.R_COMMENTS, FailureActualResult);
      resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, FailureActualResult);
      Assert.fail(FailureActualResult);

      //if(!isMethodSuccess){

      //}
     }
    }
   }


   System.out.println("suceess method name = ");
   /*
    * Common Parameter
    */
   

  } catch (Exception e) {

   // TODO Auto-generated catch block

   productValidationMethodNameKey = "DoProductValidation";
   resultMap.put(TEST_RESULT.R_METHOD_NAME, testMethodNameReport);
   resultMap.put(TEST_RESULT.R_IS_SUCCESS, false);
   resultMap.put(TEST_RESULT.R_MESSAGE, TEST_RESULT.R_IS_EXCEPTION);
   resultMap.put(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE, e.toString());
   resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, "Y");
   resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, productValidationExpectedResultAll);
   resultMap.put(TEST_RESULT.R_DESRIPTION, productValidationDescriptionAll);
   resultMap.put(TEST_RESULT.R_ACTUAL_RESULT, productValidationActualResultAll);
   testContext.setAttribute(testMethodName, resultMap);
   System.out.println("Failure method name = " + resultMap.get(TEST_RESULT.R_METHOD_NAME));

   e.printStackTrace();
   Assert.fail(e.toString());
  } finally {

   int currentValidationCount = productValidationMethodInvocationCount - 1;
   ExcelInputData excelInputData = ExcelInputData.getInstance();
   ArrayList < HashMap < String, ExcelProductValidationInputData >> excelInputProductValidatinArray = excelInputData.getExcelProductValidationInputData();
   int totalValidationCount = excelInputProductValidatinArray.size() - 1;

   System.out.println("currentValidationCount = " + currentValidationCount);
   System.out.println("totalValidationCount = " + totalValidationCount);

   if (currentValidationCount == totalValidationCount) {

    System.out.println("finally = ");
    resultMap.put(TEST_RESULT.R_METHOD_NAME, testMethodNameReport);
    resultMap.put(TEST_RESULT.R_EXPECTED_RESULT, productValidationExpectedResultAll);
    resultMap.put(TEST_RESULT.R_DESRIPTION, productValidationDescriptionAll);
    resultMap.put(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT, "Y");
    System.out.println("suceess method name = " + resultMap.get(TEST_RESULT.R_METHOD_NAME));
    System.out.println("resultMap = " + resultMap);
    testContext.setAttribute(testMethodName, resultMap);
    try {
     Thread.sleep(8000);
     prepareDriver();
    } catch (Exception e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
    }
   }

  }
 }

 private List < String > setProductNameInCollectionAndRemoveDuplicate(List < WebElement > contentList, String staticElement) {
  // TODO Auto-generated method stub
  return null;
 }

 private String callAPiAndGetResponseForSavings() throws Exception {
  System.out.println("Savings API");
  String apiRequestUrl = "https://api.loangarage.com/api/v1/single-page-wizard-product/savings-account/summary?access_token='" + Constants.MYSTORE.ACESSTOKEN + "'&page=1&lang=en_sg&country=2&limit=15&masthead_variation=light&sort%5B0%5D=cpa&sort%5B1%5D=specifications.rate&order%5B0%5D=desc&order%5B1%5D=desc";
  WebserviceRequest webserviceRequest = new WebserviceRequest();
  webserviceListener = this;
  System.out.println("Webservice Request : " + apiRequestUrl);
  String jsonResult = webserviceRequest.GET(webserviceListener, apiRequestUrl);
  if (jsonResult.isEmpty() || jsonResult == null) {
   System.out.println("Result is empty or null");
  }
  return jsonResult;
 }

 public List < String > getListOfProductValueFromWebForParticularTypeInArray(List < WebElement > contentList, String webElementLocator, String webElement) throws Exception {
  List < String > webProdArray = new ArrayList < String > ();
  for (WebElement content: contentList) {
   System.out.println("getListOfProductValueFromWebForParticularTypeInArray webElementLocator  = " + webElementLocator);
   System.out.println("getListOfProductValueFromWebForParticularTypeInArray webElement  = " + webElement);
   System.out.println("Content  = " + content);
   GetWebElement webElementObject = new GetWebElement();
   WebElement productWebElement = webElementObject.getWebElemntFromParentElemntType(content, webElementLocator, webElement);
   String productInterestRateWeb = productWebElement.getText();
   webProdArray.add(productInterestRateWeb);

   System.out.println("productInterestRateWeb  = " + productInterestRateWeb);
  }
  return webProdArray;
 }

 public List < String > getListOfProductValueFromAPIForParticularTypeInArray(JSONObject jsonResult,
  String productTypeAPINode, boolean isAnotherApiNode, String productTypeAnotherAPINode, String splitApiNodeWithASymbol, String CountryType,
  boolean isBanknameValidation, boolean isNeedTrim, String splitBankName,
  boolean isConcat, String concatType, String concatValue) throws JSONException {

  System.out.println("productTypeAPINode IN LIST ARRAY = " + productTypeAPINode);
  String[] apiNodes = productTypeAPINode.split(splitApiNodeWithASymbol);
  List < String > apiProducts = new ArrayList < String > ();
  if (jsonResult.has("premium")) {
   System.out.println("specifications available ");
   Object aObj = jsonResult.get("premium");
   if (aObj instanceof JSONObject) {
    //System.out.println(aObj);
    JSONObject premiumParams = jsonResult.getJSONObject("premium");
    JSONObject sponsoredParams = premiumParams.getJSONObject("sponsored");
    String finalProductValue = "";
    JSONObject nextJsonObject = null;
    String PrefeatureNode = "";
    if (!isBanknameValidation) {
     for (int apiNodeCount = 1; apiNodeCount <= apiNodes.length - 1; apiNodeCount++) {
      System.out.println("apiNodeCount =ok " + apiNodeCount);
      if (apiNodeCount == apiNodes.length - 1) {
       //System.out.println("nextJsonObject =ok "+nextJsonObject);
       System.out.println("PrefeatureNode =ok " + PrefeatureNode);
       if (nextJsonObject != null) {
        if (PrefeatureNode.length() > 0) {
         //	if(PrefeatureNode.equals("features")){  
         if (PrefeatureNode.equals("features") || PrefeatureNode.equals("requirements")) {
          //"value_str":"1-2 weeks","locales":null,"code":"approval_duration","is_hidden":false,"lang":"en_sg","value":null
          JSONArray nextJsonArr = nextJsonObject.getJSONArray(PrefeatureNode);
          //System.out.println("nextJsonArr = "+nextJsonArr);
          for (int featureCount = 0; featureCount < nextJsonArr.length(); featureCount++) {
           JSONObject featureJson = nextJsonArr.getJSONObject(featureCount);

           //System.out.println("featureJson = "+featureJson);

           //System.out.println("apiNodeFinalValue = "+featureJson.getString("code"));
           String codeVale = featureJson.getString("code");

           if (apiNodes[apiNodeCount].equals("initial_deposit") || apiNodes[apiNodeCount].equals("processing_fee") || apiNodes[apiNodeCount].equals(codeVale)) {

            String value = featureJson.getString("value");
            //System.out.println("Value 123 = "+value);

            if (value.equals("null") || value.equals(null) || value == null) {
             String strValue = featureJson.getString("value_str");
             System.out.println("strValue 123 = " + strValue);

             finalProductValue = strValue;
            } else {
             finalProductValue = value;
            }

           }
          }
          PrefeatureNode = "";
         } else {

          System.out.println("No features...");

         
         }

        } else {
         // System.out.println("apiNodeFinalValue = "+nextJsonObject.getString(apiNodes[apiNodeCount]));
         finalProductValue = nextJsonObject.getString(apiNodes[apiNodeCount]);
        }
       }
      } else {
       String apiSubNode = apiNodes[apiNodeCount];
       System.out.println("apiSubNode = " + apiSubNode);

       if (apiSubNode.equals("features") || apiSubNode.equals("requirements")) {

        PrefeatureNode = apiSubNode;
       }

       System.out.println("PrefeatureNode1231 = " + PrefeatureNode);

       if (nextJsonObject == null) {
        if (sponsoredParams.has(apiSubNode)) {
         Object subObject = sponsoredParams.get(apiSubNode);
         if (subObject instanceof JSONObject) {
          nextJsonObject = sponsoredParams.getJSONObject(apiSubNode);
          System.out.println("sponsoredParams = " + nextJsonObject.toString());
         } else if (subObject instanceof JSONArray) {
          JSONArray nextJsonArr = sponsoredParams.getJSONArray(apiSubNode);
          JSONObject newJson = nextJsonArr.getJSONObject(0);
          nextJsonObject = newJson;
          // finalProductValue = newJson.getString("description");
          //System.out.println("Here First ObjectnextJsonObject features = "+newJson.getString("description"));
         }
        }
       } else {
        if (nextJsonObject.has(apiSubNode)) {

         Object subObject = nextJsonObject.get(apiSubNode);
         if (subObject instanceof JSONObject) {
          nextJsonObject = nextJsonObject.getJSONObject(apiSubNode);
          System.out.println("nextJsonObject = " + nextJsonObject.toString());
         } else if (subObject instanceof JSONArray) {
          JSONObject newJsonObject = nextJsonObject.getJSONObject(apiSubNode);
          JSONArray nextJsonArr = newJsonObject.getJSONArray(apiSubNode);
          JSONObject newJson = nextJsonArr.getJSONObject(0);
          nextJsonObject = newJson;
          // finalProductValue = newJson.getString("description");
          //System.out.println("Here First ObjectnextJsonObject features = "+newJson.getString("description"));
         }
        }
       }


       if (apiSubNode.equals("features") || apiSubNode.equals("requirements")) {
        nextJsonObject = sponsoredParams;
       }
      }
     }

     String withAnotherApiValue = "";
     if (isAnotherApiNode) {
      withAnotherApiValue = getAnotherAPIValueForSponsor(jsonResult, productTypeAnotherAPINode, splitApiNodeWithASymbol, CountryType);
     }

     // System.out.println("isAnotherApiNode123 "+isAnotherApiNode);
     // System.out.println("withAnotherApiValue123 "+withAnotherApiValue);

     finalProductValue = concatIfAnyStringsNeedToConcatWithAPIResult(productTypeAPINode, CountryType, finalProductValue, withAnotherApiValue);
    } else {
     finalProductValue = sponsoredParams.getString(apiNodes[apiNodes.length - 1]);

     if (isNeedTrim) {
      if (finalProductValue.contains(splitBankName)) {


       finalProductValue = finalProductValue.split(splitBankName)[0].trim();
       System.out.println("finalProductValue.split(splitBankName)[0].trim()" + finalProductValue.split(splitBankName)[0].trim());
      }
     }

     if (isConcat) {
      if (concatType.equals("suffix")) {
       finalProductValue = finalProductValue + " " + concatValue;
      } else {
       finalProductValue = concatValue + " " + finalProductValue;
      }
     }
    }

    apiProducts.add(finalProductValue);

   } else {
    System.out.println("Empty premium value...");
   }
  }

  System.out.println("apiNodes[0] = " + apiNodes[0]);
  JSONArray productArray = jsonResult.getJSONArray(apiNodes[0]);
  for (int apiProdCount = 0; apiProdCount < productArray.length(); apiProdCount++) {
   JSONObject productData = productArray
    .getJSONObject(apiProdCount);

   String finalProductValue = "";
   JSONObject nextJsonObject = null;

   String PrefeatureNode = "";

   if (!isBanknameValidation) {
    for (int apiNodeCount = 1; apiNodeCount <= apiNodes.length - 1; apiNodeCount++) {

     if (apiNodeCount == apiNodes.length - 1) {
      if (nextJsonObject != null) {

       if (PrefeatureNode.length() > 0) {

        if (PrefeatureNode.equals("features") || PrefeatureNode.equals("requirements")) {

         //"value_str":"1-2 weeks","locales":null,"code":"approval_duration","is_hidden":false,"lang":"en_sg","value":null

         JSONArray nextJsonArr = nextJsonObject.getJSONArray(PrefeatureNode);
         //System.out.println("nextJsonArr cccc = "+nextJsonArr);nextJsonArr cccc
         for (int featureCount = 0; featureCount < nextJsonArr.length(); featureCount++) {
          JSONObject featureJson = nextJsonArr.getJSONObject(featureCount);
          //System.out.println("featureJsonwwww = "+featureJson);
          //System.out.println("apiNodeFinalValue = "+featureJson.getString("code"));
          String codeVale = featureJson.getString("code");

          //System.out.println("featureJsonwwwwsdsd = "+codeVale);
          if (apiNodes[apiNodeCount].equals("initial_deposit") || apiNodes[apiNodeCount].equals("processing_fee") || apiNodes[apiNodeCount].equals(codeVale)) {

           String value = featureJson.getString("value");
           System.out.println("Value 123 = " + value);

           if (value.equals("null") || value.equals(null) || value == null) {
            String strValue = featureJson.getString("value_str");
            System.out.println("strValue 123 = " + strValue);

            finalProductValue = strValue;
           } else {
            finalProductValue = value;
           }

          }
         }

         PrefeatureNode = "";
        } else {

         System.out.println("No features...");

        
        }


       } else {
        //System.out.println("apiNodeFinalValue = "+nextJsonObject.getString(apiNodes[apiNodeCount]));
        finalProductValue = nextJsonObject.getString(apiNodes[apiNodeCount]);
       }

      }
     } else {
      String apiSubNode = apiNodes[apiNodeCount];
      System.out.println("apiSubNode = " + apiSubNode);

      if (apiSubNode.equals("features") || apiSubNode.equals("requirements")) {

       PrefeatureNode = apiSubNode;
      }

     
      if (nextJsonObject == null) {
       if (productData.has(apiSubNode)) {
        Object subObject = productData.get(apiSubNode);
        if (subObject instanceof JSONObject) {
         nextJsonObject = productData.getJSONObject(apiSubNode);
         //System.out.println("not sponsor  = "+nextJsonObject.toString());
        } else if (subObject instanceof JSONArray) {
         JSONArray nextJsonArr = productData.getJSONArray(apiSubNode);
         JSONObject newJson = nextJsonArr.getJSONObject(0);
         nextJsonObject = newJson;
         // finalProductValue = newJson.getString("description");
         //System.out.println("Here First ObjectnextJsonObject features = "+newJson.getString("description"));
        }
       }
      } else {
       if (nextJsonObject.has(apiSubNode)) {

        Object subObject = nextJsonObject.get(apiSubNode);
        if (subObject instanceof JSONObject) {
         nextJsonObject = nextJsonObject.getJSONObject(apiSubNode);
         //System.out.println("not sponsor nextJsonObject = "+nextJsonObject.toString());
        } else if (subObject instanceof JSONArray) {
         JSONObject newJsonObject = nextJsonObject.getJSONObject(apiSubNode);
         JSONArray nextJsonArr = newJsonObject.getJSONArray(apiSubNode);
         JSONObject newJson = nextJsonArr.getJSONObject(0);
         nextJsonObject = newJson;
         // finalProductValue = newJson.getString("description");
         //System.out.println("Here First ObjectnextJsonObject features = "+newJson.getString("description"));
        }
       }
      }


      if (apiSubNode.equals("features") || apiSubNode.equals("requirements")) {
       nextJsonObject = productData;
      }
     }
    }

    String withAnotherApiValue = "";
    if (isAnotherApiNode) {
     withAnotherApiValue = getAnotherAPIValue(jsonResult, productTypeAnotherAPINode, splitApiNodeWithASymbol, CountryType, apiProdCount);
    }

    //  System.out.println("isAnotherApiNode123 "+isAnotherApiNode);
    //    System.out.println("withAnotherApiValue123 "+withAnotherApiValue);
    finalProductValue = concatIfAnyStringsNeedToConcatWithAPIResult(productTypeAPINode, CountryType, finalProductValue, withAnotherApiValue);
   } else {
    finalProductValue = productData.getString(apiNodes[apiNodes.length - 1]);

    if (isNeedTrim) {
     if (finalProductValue.contains(splitBankName)) {
      finalProductValue = finalProductValue.split(splitBankName)[0].trim();
      System.out.println("finalProductValue.split(splitBankName)[0].trimdwdw()" + finalProductValue.split(splitBankName)[0].trim());
     }
    }

    if (isConcat) {
     if (concatType.equals("suffix")) {
      finalProductValue = finalProductValue + " " + concatValue;
     } else {
      finalProductValue = concatValue + " " + finalProductValue;
     }
    }
   }

   System.out.println("finalProductValue " + finalProductValue);
   apiProducts.add(finalProductValue);
  }

  return apiProducts;
 }

 public String getAnotherAPIValueForSponsor(JSONObject jsonResult,
  String productTypeAPINode, String splitApiNodeWithASymbol, String CountryType) throws JSONException {

  System.out.println("productTypeAPINode = " + productTypeAPINode);

  String[] apiNodes = productTypeAPINode.split(splitApiNodeWithASymbol);
  System.out.println("LOGGER1" + apiNodes);
  String apiProductsAnotherValue = "";

  if (jsonResult.has("premium")) {
   System.out.println("specifications available ");
   Object aObj = jsonResult.get("premium");
   if (aObj instanceof JSONObject) {
    System.out.println(aObj);
    JSONObject premiumParams = jsonResult.getJSONObject("premium");
    JSONObject sponsoredParams = premiumParams.getJSONObject("sponsored");

    String finalProductValue = "";
    JSONObject nextJsonObject = null;
    for (int apiNodeCount = 1; apiNodeCount <= apiNodes.length - 1; apiNodeCount++) {

     if (apiNodeCount == apiNodes.length - 1) {
      if (nextJsonObject != null) {
       // System.out.println("apiNodeFinalValue string = "+nextJsonObject.getString(apiNodes[apiNodeCount]));
       finalProductValue = nextJsonObject.getString(apiNodes[apiNodeCount]);
      }
     } else {
      String apiSubNode = apiNodes[apiNodeCount];
      System.out.println("apiSubNode = " + apiSubNode);

      if (nextJsonObject == null) {
       if (sponsoredParams.has(apiSubNode)) {
        Object subObject = sponsoredParams.getJSONObject(apiSubNode);
        if (subObject instanceof JSONObject) {
         nextJsonObject = sponsoredParams.getJSONObject(apiSubNode);
         //System.out.println("sponsoredParams = "+nextJsonObject.toString());
        }
       }
      } else {
       if (nextJsonObject.has(apiSubNode)) {
        Object subObject = nextJsonObject.getJSONObject(apiSubNode);
        if (subObject instanceof JSONObject) {
         nextJsonObject = nextJsonObject.getJSONObject(apiSubNode);
         // System.out.println("nextJsonObject = "+nextJsonObject.toString());
        }
       }
      }
     }
    }
    apiProductsAnotherValue = finalProductValue;

   } else {
    System.out.println("Empty premium value...");
   }
  }

  return apiProductsAnotherValue;
 }

 public String getAnotherAPIValue(JSONObject jsonResult,
  String productTypeAPINode, String splitApiNodeWithASymbol, String CountryType, int particularProductCount) throws JSONException {

  System.out.println("productTypeAPINode = " + productTypeAPINode);

  String[] apiNodes = productTypeAPINode.split(splitApiNodeWithASymbol);
  System.out.println("LOGGER1" + apiNodes);
  String apiProductsAnotherValue = "";
  System.out.println("apiNodes[0] = " + apiNodes[0]);
  JSONArray productArray = jsonResult.getJSONArray(apiNodes[0]);
  //for (int apiProdCount = 0; apiProdCount < productArray.length(); apiProdCount++) {
  JSONObject productData = productArray
   .getJSONObject(particularProductCount);

  String finalProductValue = "";
  JSONObject nextJsonObject = null;
  for (int apiNodeCount = 1; apiNodeCount <= apiNodes.length - 1; apiNodeCount++) {

   if (apiNodeCount == apiNodes.length - 1) {

    if (nextJsonObject != null) {
     // System.out.println("apiNodeFinalValue  anotherapi = "+nextJsonObject.getString(apiNodes[apiNodeCount]));
     finalProductValue = nextJsonObject.getString(apiNodes[apiNodeCount]);
    } else {

     if (productData.has(apiNodes[apiNodeCount])) {
      Object subObject = productData.getJSONObject(apiNodes[apiNodeCount]);
      if (subObject instanceof JSONObject) {
       nextJsonObject = productData.getJSONObject(apiNodes[apiNodeCount]);
       // System.out.println("productData = "+nextJsonObject.toString());

       // System.out.println(" straight node finalProductValue = "+finalProductValue);
      } else if (subObject instanceof String) {
       finalProductValue = productData.getString(apiNodes[apiNodeCount]);
       //System.out.println(" straight node finalProductValue = "+finalProductValue);
      }
     }
    }
   } else {
    String apiSubNode = apiNodes[apiNodeCount];
    System.out.println("apiSubNode = " + apiSubNode);

    if (nextJsonObject == null) {
     if (productData.has(apiSubNode)) {
      Object subObject = productData.getJSONObject(apiSubNode);
      if (subObject instanceof JSONObject) {
       nextJsonObject = productData.getJSONObject(apiSubNode);
       // System.out.println("productData = "+nextJsonObject.toString());
      }
     }
    } else {
     if (nextJsonObject.has(apiSubNode)) {
      Object subObject = nextJsonObject.getJSONObject(apiSubNode);
      if (subObject instanceof JSONObject) {
       nextJsonObject = nextJsonObject.getJSONObject(apiSubNode);
       //System.out.println("nextJsonObject = "+nextJsonObject.toString());
      }
     }
    }
   }
  }

  apiProductsAnotherValue = finalProductValue;
  //}

  return apiProductsAnotherValue;
 }

 public HashMap < String, String > getAPINodeValueToParse(String concatValueWithOtherRequiredValue, String countryType) {
  HashMap < String, String > finalApiNodeValueToParse = new HashMap < String, String > ();

  String finalString = "";
  String isApiNeedToConcat = "N";
  String apiNodeTobeConcat = "";

  System.out.println("concatValueWithOtherRequiredValue = " + concatValueWithOtherRequiredValue);

  HashMap < String, HashMap < String, String >> getValuesFromYAML = concatIfAnyStringsNeedToConcatInYAML(concatValueWithOtherRequiredValue, countryType);

  System.out.println("getValuesFromYAML = " + getValuesFromYAML);

  if (getValuesFromYAML.containsKey(("percentage"))) {
   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("percentage");
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
   }
  } else if (getValuesFromYAML.containsKey("currency")) {
   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("currency");
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
   }
  } else if (getValuesFromYAML.containsKey("join_two_api_value")) {

   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("join_two_api_value");
   System.out.println("singleTypeValue = " + singleTypeValue);
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
    isApiNeedToConcat = "Y";
    apiNodeTobeConcat = singleTypeValue.get("apiConcatNodeValue");
   }
  } else if (getValuesFromYAML.containsKey("currency_with_two_api_value")) {

   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("currency_with_two_api_value");
   System.out.println("singleTypeValue = " + singleTypeValue);
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
    //isApiNeedToConcat = "Y";
    //apiNodeTobeConcat = singleTypeValue.get("apiConcatNodeValue");
   }
  } else if (getValuesFromYAML.containsKey("normal")) {
   // normal mode
   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("normal");
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
   }
  } else if (getValuesFromYAML.containsKey("renderBenefits")) {
   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("renderBenefits");
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
   }
  }

  ///////
  else if (getValuesFromYAML.containsKey("trans")) {
   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("trans");
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
   }
  } else if (getValuesFromYAML.containsKey("productRating")) {
   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("productRating");
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
   }
  } else if (getValuesFromYAML.containsKey("location")) {
   HashMap < String, String > singleTypeValue = getValuesFromYAML.get("location");
   if (singleTypeValue.get("apiNodeValue") != null) {
    finalString = singleTypeValue.get("apiNodeValue");
   }
  }


  System.out.println("finalString = " + finalString);
  System.out.println("isAnotherAPIConcat =" + isApiNeedToConcat);
  System.out.println("apiNodeTobeConcat = " + apiNodeTobeConcat);

  finalApiNodeValueToParse.put("apiNodeValue", finalString);
  finalApiNodeValueToParse.put("isAnotherAPIConcat", isApiNeedToConcat);
  finalApiNodeValueToParse.put("apiValueTobeConcat", apiNodeTobeConcat);

  return finalApiNodeValueToParse;
 }

 public String concatIfAnyStringsNeedToConcatWithAPIResult(String concatValueWithOtherRequiredValue, String countryType,
  String valueShouldBeConcatenated, String withValue) {
  String finalString = "";

  //System.out.println("APINodeConcatLogicType = "+APINodeConcatLogicType);

  //HashMap<String, HashMap<String, String>> getValuesFromYAML = concatIfAnyStringsNeedToConcatInYAML(concatValueWithOtherRequiredValue, countryType);
  if (APINodeConcatLogicType.contains(("percentage"))) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("percentage");
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  } else if (APINodeConcatLogicType.contains("currency")) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("currency");
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  } else if (APINodeConcatLogicType.contains("join_two_api_value")) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("join_two_api_value");
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  } else if (APINodeConcatLogicType.contains("currency_with_two_api_value")) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("currency_with_two_api_value");
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  } else if (APINodeConcatLogicType.contains("normal")) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("normal");
   // normal mode
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  }

  // Because no Concat Or Trim Logic used
  else if (APINodeConcatLogicType.contains("renderBenefits")) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("renderBenefits");
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  } else if (APINodeConcatLogicType.contains("trans")) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("trans");
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  }

  // Because no Rating used
  else if (APINodeConcatLogicType.contains("productRating")) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("productRating");
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  } else if (APINodeConcatLogicType.contains("location")) {
   HashMap < String, String > singleTypeValue = apiNoeTypeAndMapValue.get("location");
   finalString = concatLogic(singleTypeValue, valueShouldBeConcatenated, withValue);
  }


  System.out.println("concatIfAnyStringsNeedToConcatWithAPIResult = " + finalString);

  return finalString;
 }

 public String concatLogic(HashMap < String, String > singleTypeValue, String valueShouldBeConcatenated, String withAnotherAPiValue) {

  System.out.println("valueShouldBeConcatenated = " + valueShouldBeConcatenated);

  System.out.println("withAnotherAPiValue = " + withAnotherAPiValue);

  String finalString = valueShouldBeConcatenated;

  if (singleTypeValue.containsKey(("isConcat"))) {
   String isConcat = singleTypeValue.get("isConcat");
   //System.out.println("isConcat = "+isConcat);
   if (isConcat.equals("Y")) {
    String concatType = singleTypeValue.get("concatType");
    String concatValue = singleTypeValue.get("concatValue");
    String needSpace = singleTypeValue.get("needSpace");

    String isApiValueNeedToConcat = singleTypeValue.get("isApiValueNeedToConcat");
    // System.out.println("isApiValueNeedToConcat = "+withAnotherAPiValue);
    if (isApiValueNeedToConcat.equals("Y")) {
     String apiConcatNodeValue = withAnotherAPiValue;

     if (concatType.equals("prefix")) {
      if (needSpace.equals("Y")) {
       finalString = apiConcatNodeValue + valueShouldBeConcatenated;
      } else {
       finalString = apiConcatNodeValue + " " + valueShouldBeConcatenated;
      }
     } else {
      if (needSpace.equals("Y")) {
       finalString = valueShouldBeConcatenated + " " + apiConcatNodeValue;
      } else {
       finalString = valueShouldBeConcatenated + apiConcatNodeValue;
      }
     }
    } else {

     if (concatType.equals("prefix")) {
      if (needSpace.equals("Y")) {
       finalString = concatValue + " " + valueShouldBeConcatenated;
      } else {
       finalString = concatValue + valueShouldBeConcatenated;
      }
     } else {
      if (needSpace.equals("Y")) {
       finalString = valueShouldBeConcatenated + " " + concatValue;
      } else {
       finalString = valueShouldBeConcatenated + concatValue;
      }
     }
    }
   }
  }

  System.out.println("concatLogic = " + finalString);

  return finalString;
 }

 public HashMap < String, HashMap < String, String >> concatIfAnyStringsNeedToConcatInYAML(String concatValueWithOtherRequiredValue, String countryType) {

  apiNoeTypeAndMapValue = new HashMap < String, HashMap < String, String >> ();

  concatValueWithOtherRequiredValue = concatValueWithOtherRequiredValue.replace("{{", hyphenSymbol);
  concatValueWithOtherRequiredValue = concatValueWithOtherRequiredValue.replace("}}", hyphenSymbol);
  System.out.println("LLOOGGEERR_123 = " + concatValueWithOtherRequiredValue);
  String test = "%}-";
  System.out.println("12312323 = " + test);

  if (concatValueWithOtherRequiredValue.contains("%}-") && concatValueWithOtherRequiredValue.contains("|currency")) {

   String splitTheFirstColonRight = concatValueWithOtherRequiredValue.split(test)[1];
   System.out.println("LLOOGGEERR_asdf = " + concatValueWithOtherRequiredValue);
   String splittedCloseDoubleBracesDefault = "";
   if (splitTheFirstColonRight.contains("|currency")) {
    String replacedCurrency = splitTheFirstColonRight.replace("|currency", hyphenSymbol);
    splittedCloseDoubleBracesDefault = replacedCurrency.split(hyphenSymbol)[0];
    System.out.println("LLOOGGEERR = " + splitTheFirstColonRight);

    HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();

    String apiNodeOnly = "";

    apiNodeOnly = splittedCloseDoubleBracesDefault.trim().toString();
    System.out.println("CURRENCY = " + apiNodeOnly);

    System.out.println("splittedCloseDoubleBraces = " + apiNodeOnly);

    String currencyValue = "$";
    if (countryType.equals("id")) {
     currencyValue = "Rp.";

     apiNoeTypeAndNodeValue.put("needSpace", "Y");
    } else {
     apiNoeTypeAndNodeValue.put("needSpace", "N");
    }

    apiNoeTypeAndNodeValue.put("isConcat", "Y");
    apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
    apiNoeTypeAndNodeValue.put("concatType", "prefix");
    apiNoeTypeAndNodeValue.put("concatValue", currencyValue);
    apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);
    //apiNoeTypeAndNodeValue.put("apiConcatNodeValue", );

    APINodeConcatLogicType = "currency_with_two_api_value";

    apiNoeTypeAndMapValue.put("currency_with_two_api_value", apiNoeTypeAndNodeValue);
   }

  } else if (concatValueWithOtherRequiredValue.contains("-%")) {
   HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();

   String apiNodeOnly = "";
   String splittedOpenDoubleBraces = concatValueWithOtherRequiredValue.split(hyphenSymbol)[1];
   String splittedCloseDoubleBraces = splittedOpenDoubleBraces.split(hyphenSymbol)[0];

   apiNodeOnly = splittedCloseDoubleBraces.trim().toString();

   apiNoeTypeAndNodeValue.put("isConcat", "Y");
   apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
   apiNoeTypeAndNodeValue.put("concatType", "suffix");
   apiNoeTypeAndNodeValue.put("concatValue", "%");
   apiNoeTypeAndNodeValue.put("needSpace", "N");
   apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);

   apiNoeTypeAndMapValue.put("percentage", apiNoeTypeAndNodeValue);

   APINodeConcatLogicType = "percentage";
  } else if (concatValueWithOtherRequiredValue.contains("|currency")) {

   HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();

   String apiNodeOnly = "";
   String splittedOpenDoubleBraces = concatValueWithOtherRequiredValue.split(hyphenSymbol)[1];

   String replacedCurrency = splittedOpenDoubleBraces.replace("|currency", hyphenSymbol);
   String splittedCloseDoubleBraces = replacedCurrency.split(hyphenSymbol)[0];

   apiNodeOnly = splittedCloseDoubleBraces.trim().toString();
   System.out.println("CURRENCY = " + apiNodeOnly);

   System.out.println("splittedCloseDoubleBraces = " + splittedCloseDoubleBraces);

   String currencyValue = "$";
   if (countryType.equals("id")) {
    currencyValue = "Rp.";

    apiNoeTypeAndNodeValue.put("needSpace", "Y");
   } else {
    apiNoeTypeAndNodeValue.put("needSpace", "N");
   }

   apiNoeTypeAndNodeValue.put("isConcat", "Y");
   apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
   apiNoeTypeAndNodeValue.put("concatType", "prefix");
   apiNoeTypeAndNodeValue.put("concatValue", currencyValue);
   apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);

   apiNoeTypeAndMapValue.put("currency", apiNoeTypeAndNodeValue);

   APINodeConcatLogicType = "currency";
  } else if (concatValueWithOtherRequiredValue.contains("|renderBenefits")) {

   String Locator = "";
   String LocatorName = "";
   String APINodeExcel = "";

   String productValidationMethodNameKey = "DoProductValidation";

   // String isConcat = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey,"Concat");
   GetExcelInput getInput = new GetExcelInput();

 

   String newLocator = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits", "Web Element Locator");
   String newLocatorName = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits", "Web Element");
   //	String newAPINodeExcel = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits","Api Node");
   String newAPINodeExcel = "product.benefits.description";
   System.out.println("BENEFIT_APINodeExcel = " + newLocator);
   System.out.println("BENEFIT_APINodeExcel = " + newLocatorName);
   System.out.println("BENEFIT_APINodeExcel = " + newAPINodeExcel);

   HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();

 

   apiNoeTypeAndNodeValue.put("isConcat", "N");
   apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
   apiNoeTypeAndNodeValue.put("concatType", "prefix");
   apiNoeTypeAndNodeValue.put("concatValue", "");
   apiNoeTypeAndNodeValue.put("apiNodeValue", newAPINodeExcel);

   apiNoeTypeAndMapValue.put("renderBenefits", apiNoeTypeAndNodeValue);

   APINodeConcatLogicType = "renderBenefits";

  }

  /////////////////////////
  else if (concatValueWithOtherRequiredValue.contains("|trans")) {

   String Locator = "";
   String LocatorName = "";
   String APINodeExcel = "";

   String productValidationMethodNameKey = "DoProductValidation";

   // String isConcat = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method(productValidationMethodNameKey,"Concat");
   GetExcelInput getInput = new GetExcelInput();

  

   String newLocator = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits", "Web Element Locator");
   String newLocatorName = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits", "Web Element");
   //	String newAPINodeExcel = getInput.get_A_Value_Using_Key_Of_ProductValidation_Method("Benefits","Api Node");
   String newAPINodeExcel = "product.calculation_result.params.loan_tenure";
   System.out.println("Minimum Tenure newLocator = " + newLocator);
   System.out.println("Minimum Tenure newLocatorName= " + newLocatorName);
   System.out.println("Minimum Tenure newAPINodeExcel = " + newAPINodeExcel);

   HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();

   

   apiNoeTypeAndNodeValue.put("isConcat", "N");
   apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
   apiNoeTypeAndNodeValue.put("concatType", "prefix");
   apiNoeTypeAndNodeValue.put("concatValue", "");
   apiNoeTypeAndNodeValue.put("apiNodeValue", newAPINodeExcel);

   apiNoeTypeAndMapValue.put("trans", apiNoeTypeAndNodeValue);

   APINodeConcatLogicType = "trans";

  }


  ///
  else if (concatValueWithOtherRequiredValue.contains("|productRating")) {

   HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();

   String apiNodeOnly = "";
   String splittedOpenDoubleBraces = concatValueWithOtherRequiredValue.split(hyphenSymbol)[1];

   String replacedproductRating = splittedOpenDoubleBraces.replace("|productRating", hyphenSymbol);
   String splittedCloseDoubleBraces = replacedproductRating.split(hyphenSymbol)[0];

   apiNodeOnly = splittedCloseDoubleBraces.trim().toString();

   apiNoeTypeAndNodeValue.put("isConcat", "N");
   apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
   apiNoeTypeAndNodeValue.put("concatType", "prefix");
   apiNoeTypeAndNodeValue.put("concatValue", "");
   apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);

   apiNoeTypeAndMapValue.put("productRating", apiNoeTypeAndNodeValue);

   APINodeConcatLogicType = "productRating";
  }


  //**************//
  else if (concatValueWithOtherRequiredValue.contains("product.requirements.location")) {

   HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();

   String apiNodeOnly = "product.requirements.location";

   apiNoeTypeAndNodeValue.put("isConcat", "N");
   apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
   apiNoeTypeAndNodeValue.put("concatType", "prefix");
   apiNoeTypeAndNodeValue.put("concatValue", "");
   apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);

   apiNoeTypeAndMapValue.put("location", apiNoeTypeAndNodeValue);

   APINodeConcatLogicType = "location";
  }

  
  else if (concatValueWithOtherRequiredValue.contains("- -")) {

   System.out.println("LLOOGGEERR_0000 = " + concatValueWithOtherRequiredValue);


   


   if (concatValueWithOtherRequiredValue.contains("is defined ?") && concatValueWithOtherRequiredValue.contains("|ucfirst")) {
    HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();
    //String apiNodeOnly= "";			
    String splitTheFirstValue = concatValueWithOtherRequiredValue.split("- -")[0];
    String splitTheSecondValue = concatValueWithOtherRequiredValue.split("- -")[1];

    String firstDefinedValeReplaceVal = splitTheFirstValue.replace("is defined ?", hyphenSymbol);
    String firstDefinedValeTrimVal = firstDefinedValeReplaceVal.split(hyphenSymbol)[1];
    String secondDefinedValeVal = firstDefinedValeTrimVal.split(hyphenSymbol)[0];
    secondDefinedValeVal = secondDefinedValeVal.toString().trim();

    // second api val 
    String replacedSecondNodeVal = splitTheSecondValue.replace("|ucfirst", hyphenSymbol);
    String secondSpleVal = replacedSecondNodeVal.split(hyphenSymbol)[0];
    secondSpleVal = secondSpleVal.toString().trim();

    apiNoeTypeAndNodeValue.put("isConcat", "Y");
    apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "Y");
    apiNoeTypeAndNodeValue.put("concatType", "suffix");
    apiNoeTypeAndNodeValue.put("concatValue", "");
    apiNoeTypeAndNodeValue.put("needSpace", "Y");
    apiNoeTypeAndNodeValue.put("apiNodeValue", secondDefinedValeVal);
    apiNoeTypeAndNodeValue.put("apiConcatNodeValue", secondSpleVal);
    apiNoeTypeAndMapValue.put("join_two_api_value", apiNoeTypeAndNodeValue);
    APINodeConcatLogicType = "join_two_api_value";



   } else {
    HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();
    //String apiNodeOnly= "";			
    String splitTheFirstValue = concatValueWithOtherRequiredValue.split("- -")[0];
    String splitTheSecondValue = concatValueWithOtherRequiredValue.split("- -")[1];
    String splittedFirstOpenBraceValue = splitTheFirstValue.split(hyphenSymbol)[1];
    String splittedFirstVal = splittedFirstOpenBraceValue.trim().toString();
    String splittedSecondCloseBraceValue = splitTheSecondValue.split(hyphenSymbol)[0];
    String splittedSecondVal = splittedSecondCloseBraceValue.trim().toString();
    //System.out.println("splittedFirstVal = "+splittedFirstVal);
    ///System.out.println("splittedSecondVal = "+splittedSecondVal);
    apiNoeTypeAndNodeValue.put("isConcat", "Y");
    apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "Y");
    apiNoeTypeAndNodeValue.put("concatType", "suffix");
    apiNoeTypeAndNodeValue.put("concatValue", "");
    apiNoeTypeAndNodeValue.put("needSpace", "Y");
    apiNoeTypeAndNodeValue.put("apiNodeValue", splittedFirstVal);
    apiNoeTypeAndNodeValue.put("apiConcatNodeValue", splittedSecondVal);
    apiNoeTypeAndMapValue.put("join_two_api_value", apiNoeTypeAndNodeValue);
    APINodeConcatLogicType = "join_two_api_value";
   }
  } else {
   HashMap < String, String > apiNoeTypeAndNodeValue = new HashMap < String, String > ();

   String apiNodeOnly = "";
   

   String splittedOpenDoubleBraces = concatValueWithOtherRequiredValue.split(hyphenSymbol)[1];
   String splittedCloseDoubleBraces = splittedOpenDoubleBraces.split(hyphenSymbol)[0];

   apiNodeOnly = splittedCloseDoubleBraces.trim().toString();

   apiNoeTypeAndNodeValue.put("isConcat", "N");
   apiNoeTypeAndNodeValue.put("isApiValueNeedToConcat", "N");
   apiNoeTypeAndNodeValue.put("concatType", "suffix");
   apiNoeTypeAndNodeValue.put("concatValue", "%");
   apiNoeTypeAndNodeValue.put("needSpace", "N");
   apiNoeTypeAndNodeValue.put("apiNodeValue", apiNodeOnly);

   apiNoeTypeAndMapValue.put("normal", apiNoeTypeAndNodeValue);

   APINodeConcatLogicType = "normal";
  }

  return apiNoeTypeAndMapValue;
 }

 public double roundAValue(String roundDecimal, String roundValue) {
  double finalRoundValue = 0.0;
  DecimalFormat decimalFormat = null;

  System.out.println("Value before round logic = " + roundValue);

  if (roundDecimal.equals("1")) {
   decimalFormat = new DecimalFormat("#.#");
  } else if (roundDecimal.equals("2")) {
   decimalFormat = new DecimalFormat("#.##");
  } else if (roundDecimal.equals("3")) {
   decimalFormat = new DecimalFormat("#.###");
  } else if (roundDecimal.equals("4")) {
   decimalFormat = new DecimalFormat("#.####");
  } else if (roundDecimal.equals("5")) {
   decimalFormat = new DecimalFormat("#.#####");
  } else if (roundDecimal.equals("6")) {
   decimalFormat = new DecimalFormat("#.######");
  }

  decimalFormat.setRoundingMode(RoundingMode.CEILING);

  Double roundingValue = Double.parseDouble(roundValue);
  String roundedValue = decimalFormat.format(roundingValue);
  finalRoundValue = Double.parseDouble(roundedValue);

  System.out.println("Value after round logic = " + roundedValue);
  return finalRoundValue;
 }

 
 // *  Custom methods

 public String createWebserviceRequestUrl(HashMap < String, ArrayList < String >> filtersKeyValue, int apiInvocationCount) {

  String channelURL = "";
  String apiUrl = "";
  try {

   ExcelInputData excelinputobj = ExcelInputData.getInstance();

   Object configobjYAML = excelinputobj.getYAMLConfigData();
   Map configmap = (Map) configobjYAML;

   System.out.println("configyaml" + configmap);

   Object appobject = configmap.get("app");

   Map appmap = (Map) appobject;

   Object hostobject = appmap.get("hosts");

   Map hostmap = (Map) hostobject;

   String apivalues = (String) hostmap.get("api");
   System.out.println("apivalues = " + apivalues);

   Object productobjYAML = excelinputobj.getYAMLData();
   Map productmap = (Map) productobjYAML;

   Object tablefields = productmap.get("table_fields");
   Map tablefieldsmap = (Map) tablefields;

   Object sorting = tablefieldsmap.get("sorting_params");
   Map sortingmap = (Map) sorting;

   String channelname = "";


   Object firstObject = "";
   if (channelname.contains("personal-loan")) {

    Object mandatory = sortingmap.get("mandatory");
    List < Map > dashArrayUnsecure = (ArrayList < Map > ) mandatory;
    Object firstObjectUnsecure = dashArrayUnsecure.get(0);
    Object secondObjectUnsecure = dashArrayUnsecure.get(1);

    System.out.println("firstObjectUnsecure = " + firstObjectUnsecure);
    System.out.println("secondObjectUnsecure = " + secondObjectUnsecure);

   } else {

    Object main = sortingmap.get("main");
    List < Map > dashArray = (ArrayList < Map > ) main;
    firstObject = dashArray.get(0);
    Map firstMap = (Map) firstObject;

    String sortby = (String) firstMap.get("sort");
    String order = (String) firstMap.get("order");

    System.out.println("sortby = " + sortby);
    System.out.println("order = " + order);

   }


   if (productmap.get("route") instanceof String)
    channelname = (String) productmap.get("route");
   else
    channelname = (String)((ArrayList) productmap.get("route")).get(0);

   System.out.println("channelname = " + channelname);

   Object filterval = productmap.get("filter");
   Map filtervalmap = (Map) filterval;

   Object defaultval = filtervalmap.get("default_values");
   ExcelInputData excelInputData = ExcelInputData.getInstance();
   GetExcelInput getInput = new GetExcelInput();

   String apichannels = "";
   apichannels = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, "Api Channel Name");

   channelURL = getInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, "URL");

   System.out.println("apichannels : " + apichannels);
   Map defaultvalsmap = (Map) defaultval;

   String str = "";

   if (channelCountry.contains("sg")) {

    if (channelURL.equals("http://www.moneysmart.sg/travel-insurance")) {
     str = "" + apivalues + "/api/v1/single-page-wizard-product/" + apichannels + "/single-trip?premium=1&calculate=1&page=1&lang=en_sg&country=2&limit=15&masthead_variation=light";
    } else if (channelURL.equals("http://www.moneysmart.sg/annual-travel-insurance")) {
     str = "" + apivalues + "/api/v1/single-page-wizard-product/" + apichannels + "/annual?premium=1&calculate=1&page=1&lang=en_sg&country=2&limit=15&masthead_variation=light";
    } else {
     str = "" + apivalues + "/api/v1/single-page-wizard-product/" + apichannels + "/summary?premium=1&calculate=1&page=1&lang=en_sg&country=2&limit=15&masthead_variation=light";
    }

   } else {
	   
	   if(channelURL.equals("https://www.duitpintar.com/en_sg/travel-insurance")){
		   str = "" + apivalues + "/api/v1/single-page-wizard-product/" + apichannels + "/single-trip?premium=1&calculate=1&page=1&lang=id_id&country=1&limit=15&masthead_variation=light";
	   }
	   else{
		   str = "" + apivalues + "/api/v1/single-page-wizard-product/" + apichannels + "/summary?premium=1&calculate=1&page=1&lang=id_id&country=1&limit=15&masthead_variation=light";
	   }
   }



   GetExcelInput getAMethod = new GetExcelInput();
   HashMap < String, String > excelToFilterKey = new HashMap < > ();

   for (Object key: filtersKeyValue.keySet()) {
    String keyval = (String) key;
    String keyFromExcel = getAMethod.get_A_Value_Using_Key_Of_Filters_Method(keyval, "API Key In YAML");

    // if(){
    excelToFilterKey.put(keyFromExcel, keyval);
    //}

    //excelToFilterKey.put("citizenship", "Citizenship");
   }
   Map firstMsortingvaluesap = (Map) firstObject;
   for (Object Key: ((Map) firstObject).keySet()) {
    String sortval = (String) Key;

    str += "&" + sortval + "=" + ((Map) firstObject).get(sortval);

   }

   for (Object key: defaultvalsmap.keySet()) {

    String keyval = (String) key;
    System.out.println("KEY : " + keyval);
    System.out.println("VAL: " + defaultvalsmap.get(keyval));

    if (!keyval.equals("loan_tenure_unit") && !keyval.equals("period_unit") && !keyval.equals("with_lock_in")) {
     System.out.println("KEY : " + keyval);
     System.out.println("channelname : " + channelname);

     if (channelname.equalsIgnoreCase("fixed-deposit") || channelname.contains("Fixed") || channelname.contains("fixed")) {
      String actualValue = filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
      System.out.println("KEY PEriond: " + keyval);
      if (keyval.equals("period")) {
       actualValue = actualValue.replace("month", hyphenSymbol);
       actualValue = actualValue.split(hyphenSymbol)[0].trim();
       System.out.println("KEY PERIOD: " + actualValue.toString().trim());
       str += "&filters[" + keyval + "]=" + actualValue.toString().trim();
      } else {
       str += "&filters[" + keyval + "]=" + actualValue;
      }
     } else if (channelname.equalsIgnoreCase("education-loan") || channelname.contains("Education") || channelname.contains("education")) {
      String actualValue = filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
      System.out.println("KEY PEriond: " + keyval);
      System.out.println("KEY PEriond: " + actualValue);
      if (actualValue.equalsIgnoreCase("Singapore") || (actualValue.contains("Singapore"))) {
       str += "&filters[" + keyval + "]=" + "local";
       System.out.println("KEY PEriond: 114 " + str);
      } else {
       str += "&filters[" + keyval + "]=" + actualValue;
       System.out.println("KEY PEriond: 112 " + str);
      }
     }
     
     else {

      if (channelCountry.contains("sg")) {

       System.out.println("keyvalkeyvalkeyval = " + keyval);
       System.out.println("gwt = " + excelToFilterKey);
       System.out.println("gvalwt = " + excelToFilterKey.get(keyval));
       System.out.println("gwt xdfsdfs = " + filtersKeyValue.get(excelToFilterKey.get(keyval)));
       System.out.println("filtersKeyValue = " + filtersKeyValue);

       if (excelToFilterKey.get(keyval) != null) {



        if (keyval.equals("countries")) {
         String singleCommaCountry = filtersKeyValue.get(excelToFilterKey.get("countries")).get(apiInvocationCount);

         String allCountry = Splitcountry(singleCommaCountry);

         System.out.println("allCountry = " + allCountry);
         str += allCountry;

         System.out.println("str = " + str);

        } else if (keyval.equals("departure_date")) {


         if (pageUrl.equals("http://www.moneysmart.sg/annual-travel-insurance")) {

          depaturedate = getWebDriver().findElement(By.name("filters[coverage_start_date]")).getAttribute("value");

         } else {
          depaturedate = getWebDriver().findElement(By.name("filters[departure_date]")).getAttribute("value");
         }

         System.out.println("depaturedate" + depaturedate);


         str += "&filters[" + keyval + "]=" + depaturedate;


        } else if (keyval.equals("arrival_date")) {

         Thread.sleep(2000);

         arrivaldate = getWebDriver().findElement(By.name("filters[arrival_date]")).getAttribute("value");
         System.out.println("arrivaldate" + arrivaldate);
         str += "&filters[" + keyval + "]=" + arrivaldate;
         System.out.println("str" + str);


        } 
        else {
         str += "&filters[" + keyval + "]=" + filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
        }

       } else {

        if (keyval.equals("passengers")) {

         str += "&filters[passengers][adult]=" + filtersKeyValue.get(excelToFilterKey.get("adult")).get(apiInvocationCount);
         str += "&filters[passengers][child]=" + filtersKeyValue.get(excelToFilterKey.get("child")).get(apiInvocationCount);
        }

        else {
         str += "&filters[" + keyval + "]=" + defaultvalsmap.get(keyval);
        }


       }


       System.out.println("filtersKeyValue = " + str);


      } else {

       if (pageUrl.contains("kredit-multiguna")) {

        if (keyval.contains("collateral_type")) {
         str += "&filters[" + keyval + "]=" + DuitpintarcollateralId;
        } else if (keyval.contains("loan_purpose")) {
         str += "&filters[" + keyval + "]=" + Duitpintarloan_purposeId;
        } else {
         str += "&filters[" + keyval + "]=" + filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
        }

        System.out.println("strProvider = " + str);
        System.out.println("keyvalkeyvalkeyval = " + keyval);
        System.out.println("filtersKeyValue = " + filtersKeyValue);
       }


       if (pageUrl.equals("http://www.moneysmart.sg/travel-insurance")) {
      
       } else {

        if (keyval.contains("provider")) {
         str += "&filters[" + keyval + "]=" + DuitpintarProviderId;
        } else if (keyval.contains("employment")) {
         str += "&filters[" + keyval + "]=" + DuitpintaremploymentId;
        } else {
         str += "&filters[" + keyval + "]=" + filtersKeyValue.get(excelToFilterKey.get(keyval)).get(apiInvocationCount);
        }

        System.out.println("strProvider = " + str);
        System.out.println("keyvalkeyvalkeyval = " + keyval);
        System.out.println("filtersKeyValue = " + filtersKeyValue);
       }


      }
     }
    } else
     str += "&filters[" + keyval + "]=" + defaultvalsmap.get(keyval);
   }

   String FinalAPI = str + "&access_token=" + Constants.MYSTORE.ACESSTOKEN;
  
   apiUrl = FinalAPI;
  System.out.println("apiUrl = " + apiUrl);
  System.out.println("filtersKeyValue = " + filtersKeyValue);
  if (filtersKeyValue.containsKey("Studying")) {
    System.out.println("For Studying1 = " + filtersKeyValue.get("Studying").get(apiInvocationCount));
    System.out.println("trim = " + filtersKeyValue.get("Studying").toString().trim());
   } else {
    System.out.println("For null = ");
   }

  } catch (Exception e) {
   e.printStackTrace();
  }

  if (apiUrl.contains("Show Priority Banking Packages") || apiUrl.contains("No Lock-in packages only")) {
   if (apiUrl.contains("Show Priority Banking Packages")) {
    apiUrl = apiUrl.replace("Show Priority Banking Packages", "");
   }
   if (apiUrl.contains("No Lock-in packages only")) {
    apiUrl = apiUrl.replace("No Lock-in packages only", "No");
   }
  } else {
   if (apiUrl.contains("Priority Bank Options") || apiUrl.contains("Lock-in")) {
    if (apiUrl.contains("Priority Bank Options")) {
     apiUrl = apiUrl.replace("Priority Bank Options", "");
    }
    if (apiUrl.contains("Lock-in")) {
     apiUrl = apiUrl.replace("Lock-in", "Yes");
    }
   }
  }


  if (apiUrl.contains("All")) {
   apiUrl = apiUrl.replace("All", "");
  }
  if (apiUrl.contains("Board Rate")) {
   apiUrl = apiUrl.replace("Board Rate", "BOARD");
  }
  if (apiUrl.contains("SIBOR Rate")) {
   apiUrl = apiUrl.replace("SIBOR Rate", "SIBOR");
  }
  if (apiUrl.contains("SOR Rate")) {
   apiUrl = apiUrl.replace("SOR Rate", "SOR");
  }
  if (apiUrl.contains("SIBOR/SOR Combo Rate")) {
   apiUrl = apiUrl.replace("SIBOR/SOR Combo Rate", "combo");
  }

if (apiUrl.contains("Both")) {
   apiUrl = apiUrl.replace("Both", "");
  }
  if (apiUrl.contains("Floating Rate")) {
   apiUrl = apiUrl.replace("Floating Rate", "floating");
  }
  if (apiUrl.contains("Under Construction")) {
   apiUrl = apiUrl.replace("Under Construction", "uncompleted");
  }
  if (apiUrl.contains("Completed")) {
   apiUrl = apiUrl.replace("Completed", "completed");
  }
  if (apiUrl.contains("Condo / Apartment")) {
   apiUrl = apiUrl.replace("Condo / Apartment", "condo");
  }
  if (apiUrl.contains("HDB")) {
   apiUrl = apiUrl.replace("HDB", "hdb");
  }
  if (apiUrl.contains("Executive Condo")) {
   apiUrl = apiUrl.replace("Executive Condo", "executive");
  }
  if (apiUrl.contains("Landed Property")) {
   apiUrl = apiUrl.replace("Landed Property", "landed");
  }

  if (channelURL.contains("/refinancing")) {
   if (apiUrl.contains("filters[property_status]=")) {
    apiUrl = apiUrl.replace("filters[property_status]=", "filters[property_status]=completed");
   }
  }



  if (apiUrl.contains("filters[countries]")) {
   apiUrl = apiUrl.replace("filters[countries]", "filters[countries][]");
  }
  
  
  if (apiUrl.contains("Domestic Trip")) {
	   apiUrl = apiUrl.replace("Domestic Trip", "domestic");
	  }
  
  if (apiUrl.contains("Overseas Trip")) {
	   apiUrl = apiUrl.replace("Overseas Trip", "overseas");
	  }


  Constants.MYSTORE.SINGLEPRODUCTJSONAPI = apiUrl;

  System.out.println("apiUrlafterreplace = " + apiUrl);

  return apiUrl;
 }


 //	    Calendar methods starts
 public void selectCalendarone(String monthandyear, String startday1) throws Exception {
  
  int chk = 0;
  do {
   System.out.println("inside calendar method");
   Thread.sleep(2000);



  
   List < WebElement > startmonthandyears = getWebDriver().findElements(By.xpath("//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/thead/tr[2]/th[2]"));
  
   Thread.sleep(2000);
   System.out.println("pgm started");
   System.out.println(startmonthandyears.size());
   for (int j = 0; j < startmonthandyears.size(); j++) {

    System.out.println("insidefor");
    System.out.println(startmonthandyears.get(j).getText());

    if (startmonthandyears.get(j).getText().equals(monthandyear)) {
     System.out.println("pass case");
     selectstartDate(startday1);
     return;
     //System.out.println(startmonthandyears.get(j).getText());            
    } else {
     getWebDriver().findElement(By.xpath("//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/thead/tr[2]/th[3]")).click();
     

     Thread.sleep(2000);
     chk++;
     System.out.println(chk);
     break;
    }
   }
  } while (chk <= 12);

  System.out.print(chk);
  return;

 }

 public void selectstartDate(String startday) throws InterruptedException, Exception {
  
  List < WebElement > startdays = getWebDriver().findElements(By.xpath("//div[@class='datepicker datepicker-dropdown dropdown-menu datepicker-orient-left datepicker-orient-top']/div[@class='datepicker-days']/table/tbody/tr/td"));

  Thread.sleep(2000);
  
  for (int i = 0; i < startdays.size(); i++) {
   System.out.println(startdays.get(i).getText());
   
   if (startdays.get(i).getText().equals(startday)) {

    startdays.get(i).click();

    Thread.sleep(2000);
    System.out.println("upto click");
    break;

    //new Actions(driver).moveToElement(element).click().perform()               
   }
  }
 }

 /// calendar methods ends  


 public String Splitcountry(String countryValue) {

  String[] Countryarray = countryValue.split(",");
  String Allfiltercountries = "";

  for (int countrycount = 0; countrycount < Countryarray.length; countrycount++) {

   System.out.println("contryvalue " + Countryarray[countrycount]);

   if (Allfiltercountries.length() == 0) {

    Allfiltercountries = "&filters%5Bcountries%5D%5B%5D=" + Countryarray[countrycount];
   } else {
    Allfiltercountries = Allfiltercountries + "&" + "filters%5Bcountries%5D%5B%5D=" + Countryarray[countrycount];
   }
  }

  return Allfiltercountries;
 }

 public void setAPIResult(String apiResponse) {
  JSONObject webResult = null;
  try {
   webResult = new JSONObject(apiResponse);
  } catch (JSONException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
  this.webserviceApiResult = webResult;
 }

 public JSONObject getAPIResult() {
  return webserviceApiResult;
 }

 public void setCalculationCount(int calculationCount) {
  this.totalCalculationCount = calculationCount;
 }

 public int getCalculationCount() {
  return totalCalculationCount;
 }

 @DataProvider(name = "getAPIResultFromDataProvider")
 public Object[][] getAPIResultFromDataProvider() {
  Object[][] jsonObject = new Object[1][1];
  jsonObject[0][0] = getAPIResult();
  return jsonObject;
 }

 @Override
 public void webserviceRequestSuccessListener(int statusCode, String statusMessage, String apiResult) {
  // TODO Auto-generated method stub
 }

 @Override
 public void webserviceRequestFailureListener(int statusCode, String statusMessage) {
  // TODO Auto-generated method stub
 }

//////////////////////////////////////////// 	Custom Methods  ///////////////////////////////////////


 public static String parseDate(String time, String _inputpattern, String _outputpattern) {
  // time = "2015-02-24";
  // String inputPattern = "yyyy-MM-dd";
  // outputPattern = "MMM dd, yyyy";

  String inputPattern = _inputpattern;
  String outputPattern = _outputpattern;

  SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
  SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

  Date date = null;
  String str = null;


  try {
   date = inputFormat.parse(time);
   str = outputFormat.format(date);
  } catch (java.text.ParseException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }


  return str;
 }


}