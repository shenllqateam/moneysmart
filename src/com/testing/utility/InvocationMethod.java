package com.testing.utility;
/**
 * 
 * @author Shenll Technologies Solutions
 *
 */
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.annotations.ITestAnnotation;
import org.testng.internal.annotations.IAnnotationTransformer;
import org.testng.internal.annotations.ITest;

import com.model.GetExcelInput;

/*
 * This class will invoke the methods multiple time based on the invocation count which set in TestNG main class. 
 * In this project RunSingleSuiteTest.java is the class which will run TestNG classes
 */

public class InvocationMethod implements IAnnotationTransformer {
	/*
	 * By default, invocation count is 1.
	 */
	int calculationMethodInvocationCount = 1;
	int filterMethodInvocationCount = 1;
	int productValidationMethodInvocationCount = 1;
	
	public InvocationMethod(int calculationMethodInvocationCount, int filterMethodInvocationCount,
			int productValidationMethodInvocationCount){
		this.calculationMethodInvocationCount = calculationMethodInvocationCount;
		this.filterMethodInvocationCount = filterMethodInvocationCount;
		this.productValidationMethodInvocationCount = productValidationMethodInvocationCount;
	}

	@Override
	public void transform(ITestAnnotation annotation, Class testClass,
		      Constructor testConstructor, Method testMethod)
	  {
		
		
		if ("DoNLFfilters".equals(testMethod.getName())) {
		    	
		    	annotation.setInvocationCount(filterMethodInvocationCount);
		}
		
		if ("DoFilterAPICall".equals(testMethod.getName())) {
	    	
	    	annotation.setInvocationCount(calculationMethodInvocationCount);
		}
		
		if ("DoProductValidation".equals(testMethod.getName())) {
	    	
	    	annotation.setInvocationCount(productValidationMethodInvocationCount);
		}
	    
	  }
}
