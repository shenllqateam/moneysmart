package com.testing.utility;
/**
 * 
 * @author Shenll Technologies Solutions
 *
 */
import java.io.PrintWriter;
import java.util.Comparator;

import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;

public class TestMethodsResult {
	
	private int methodPriority;
	private ITestNGMethod method;
	private ITestContext testContext;
	private ISuite suite;
	private IResultMap tests;
	private String testname;
	private String style;
	private String details;
	private PrintWriter writer;
	private String browserName;
	
	public TestMethodsResult(){
		
	}

	public void setMethodPriority(int methodPriority){
		this.methodPriority = methodPriority;
	}
	
	public int getMethodPriority(){
		return methodPriority;
	}
	
	public void setTestContext(ITestContext testContext){
		this.testContext = testContext;
	}
	
	public ITestContext getTestContext(){
		return testContext;
	}
	
	public void setMethod(ITestNGMethod method){
		this.method = method;
	}
	
	public ITestNGMethod getMethod(){
		return method;
	}
	
	public void setSuite(ISuite suite){
		this.suite = suite;
	}
	
	public ISuite getSuite(){
		return suite;
	}
	
	public void setResultMap(IResultMap tests){
		this.tests = tests;
	}
	
	public IResultMap getResultMap(){
		return tests;
	}
	
	public void setTestName(String testname){
		this.testname = testname;
	}
	
	public String getTestName(){
		return testname;
	}
	
	public void setStyle(String style){
		this.style = style;
	}
	
	public String getStyle(){
		return style;
	}
	
	public void setDetails(String details){
		this.details = details;
	}
	
	public String getDetails(){
		return details;
	}
	
	public void setPrinter(PrintWriter writer){
		this.writer = writer;
	}
	
	public PrintWriter getPrinter(){
		return writer;
	}
	
	public void setBrowserName(String browserName){
		this.browserName = browserName;
	}
	
	public String getBrowserName(){
		return browserName;
	}
	
	
	/*Comparator for sorting the list by roll no*/
    public static Comparator<TestMethodsResult> MethodPriority = new Comparator<TestMethodsResult>() {

	public int compare(TestMethodsResult s1, TestMethodsResult s2) {

	   int priorityno1 = s1.getMethodPriority();
	   int priorityno2 = s2.getMethodPriority();

	   /*For ascending order*/
	   return priorityno1-priorityno2;

	   /*For descending order*/
	   //priorityno2-priorityno1;
   }};
    @Override
    public String toString() {
        return "[ methodPriority=" + methodPriority + ", " +
        		  " method=" + method + "," +
        		  " testContext=" + testContext + "," +
        		  " suite=" + suite + "," +
        		  " tests=" + tests + "," +
        		  " testname=" + testname + "," +
        		  " style=" + style + "," +
        		  " details=" + details + "," +
        		  " writer=" + writer + 
        				"]";
    }
}
