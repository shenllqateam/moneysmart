package com.testing.utility;
/**
 * 
 * @author Shenll Technologies Solutions
 *
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.IInvokedMethod;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestClass;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.collections.Lists;
import org.testng.internal.ResultMap;
import org.testng.internal.Utils;
import org.testng.xml.XmlSuite;

import com.common.utility.FTPFileUploadListener;
import com.common.utility.FTPFileUploadTask;
import com.common.utility.SendEmailTask;
import com.common.utility.SendEmailTaskListener;
import com.google.common.collect.Iterators;
import com.model.Constants.EXCEL_METHODS_INPUT;
import com.model.Constants.TEST_RESULT;
import com.model.GetExcelInput;






import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Created by Shenll Software Solutions.
 */
public class TestNGCustomReportListener implements IReporter, FTPFileUploadListener,SendEmailTaskListener{
	
	FTPFileUploadListener uploadNotifier;
	SendEmailTaskListener emailSendListener;
	
	/*
	 * Email send variables
	 */
	private String MAIL_AUTH_USERNAME = "";
	private String MAIL_AUTH_PASSWORD = "";
	
	private String MAIL_FROM_EMAIL_ID = "";
	private String MAIL_FROM_EMAIL_PASSWORD = "";
	
	private String MAIL_TO_EMAIL_ID = "";
	private String MAIL_CC_USER = "";
	private String MAIL_CC_EMAIL = "";
	
	private String MAIL_SUBJECT = "MoneySmart";

	private String MAIL_TO_USERNAME = "";
	private String MAIL_BODY_MESSAGE = "MoneySmart"; 
	private String MAIL_FROM_USERNAME = "";
	
	private String MAIL_ATTACHMENT_TYPE = "text/html";
	
	private PrintWriter mailWriter; 
	private PrintWriter uploadWriter;
	private int m_row;
	private Integer m_testIndex;
	private int m_methodIndex;
	
	private String reportSummaryTitle= "Channel Test Summary";
	private String reportTitle= "Channel Test Report";
	
	//private static final String GENERATED_REPORT_PATH = "/var/lib/jenkins/workspace/moneysmartfirst/test-output";
	
	private static final String GENERATED_REPORT_PATH = "E:/project/MoneyDetails/test-output/";
	private String REPORT_UPLOAD_FILE_NAME = "-qa-report-";
	private String REPORT_EMAIL_FILE_NAME = "-qa-mail-report-";
	private String uniqueReportId ="";
	
	Date dNow = new Date();
	SimpleDateFormat ft = new SimpleDateFormat(
			"yyyy-MM-dd_hh-mm-ss");
	ArrayList<TestMethodsResult> resultArray = new ArrayList<TestMethodsResult> ();
	ArrayList<TestMethodsResult> resultFailedArray = new ArrayList<TestMethodsResult> ();
	
	/*
	 * Excel Config Key
	 */
	
	String mailConifgKey = "Reporting Email";
	String fromMailIdFromExcel = "From Email";
	String fromEmailPasswordFromExcel = "From Email Password";
	String toMailIdFromExcel = "To Email";
	String fromUsernameFromExcel = "From Username";
	String toUsernameFromExcel = "To Username";
	String ccUsernameFromExcel = "CC Username";
	String ccMailIdFromExcel = "CC Email";
	
	String ChannelName ="";
	
	GetExcelInput excelInput;
	/** Creates summary of the run */
	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
			String outdir) {
		  
		excelInput = new GetExcelInput();
		
		uniqueReportId =genRandomUniqueNumber();
		String _ChannelName = excelInput.get_A_Value_Using_Key_Of_A_Method(EXCEL_METHODS_INPUT.Openwebpage_MethodNameKey, EXCEL_METHODS_INPUT.Openwebpage_ChaneelNameKey);
	    // ChannelName = "Multi Purpose Loan";
		ChannelName = _ChannelName.replace("-", " ");
		
		ChannelName = ChannelName.substring(0, 1).toUpperCase() + ChannelName.substring(1);
		System.out.println("ChannelName = "+ChannelName);
		
		
		String FromEmail = excelInput.get_A_Value_Using_Key_Of_A_Method(mailConifgKey, fromMailIdFromExcel);
		String FromEmailPassword = excelInput.get_A_Value_Using_Key_Of_A_Method(mailConifgKey, fromEmailPasswordFromExcel);
		String ToEmail = excelInput.get_A_Value_Using_Key_Of_A_Method(mailConifgKey, toMailIdFromExcel);
		String FromUsername = excelInput.get_A_Value_Using_Key_Of_A_Method(mailConifgKey, fromUsernameFromExcel);
		String ToUsername = excelInput.get_A_Value_Using_Key_Of_A_Method(mailConifgKey, toUsernameFromExcel);
		String CCUsername = excelInput.get_A_Value_Using_Key_Of_A_Method(mailConifgKey, ccUsernameFromExcel);
		String CCEmail = excelInput.get_A_Value_Using_Key_Of_A_Method(mailConifgKey, ccMailIdFromExcel);
		
		REPORT_UPLOAD_FILE_NAME = ChannelName +REPORT_UPLOAD_FILE_NAME + uniqueReportId + ".html";
		REPORT_EMAIL_FILE_NAME = ChannelName +REPORT_EMAIL_FILE_NAME + uniqueReportId + ".html";
		
		reportSummaryTitle  = ChannelName +reportSummaryTitle;
		reportTitle  = ChannelName +reportTitle;
		
		MAIL_BODY_MESSAGE = MAIL_BODY_MESSAGE+" "+ChannelName+ " Test Report";
		MAIL_SUBJECT = MAIL_SUBJECT+" "+ChannelName+" Test Report";
		
		MAIL_FROM_EMAIL_ID = FromEmail;
		MAIL_FROM_EMAIL_PASSWORD = FromEmailPassword;
		MAIL_TO_EMAIL_ID = ToEmail;
		MAIL_CC_EMAIL = CCEmail;
		MAIL_CC_USER = CCUsername;

		
		MAIL_FROM_USERNAME  = FromUsername;
		MAIL_TO_USERNAME  = ToUsername;
		
		
		
		try {
			uploadWriter = createUploadWriter(outdir);
			mailWriter = createMailWriter(outdir);
		} catch (IOException e) {
			System.err.println("Unable to create output file");
			e.printStackTrace();
			return;
		}

		/*
		 * Upload report 
		 */
		startHtml(uploadWriter);
		upload_writeReportTitle(reportTitle);
		
		uploadWriter.println("</table>");
		upload_generateMethodSummaryReport(suites);
		
		
		endHtml(uploadWriter);
		uploadWriter.flush();
		uploadWriter.close();
		
		/*
		 * Mail report 
		 */
		startHtml(mailWriter);
		mail_writeReportTitle(reportSummaryTitle);
		mail_generateSuiteSummaryReport(suites);
		mail_viewSummaryReportInAlink("summaryLink");
		endHtml(mailWriter);
		mailWriter.flush();
		mailWriter.close();
		
		/*
		 * Start upload task after report generated
		 */
		
		FTPFileUploadTask uploadFileToServer = new FTPFileUploadTask();
		uploadNotifier = this;
		uploadFileToServer.uploadThisFileToServer(uploadNotifier,GENERATED_REPORT_PATH,REPORT_UPLOAD_FILE_NAME);
	}

	protected PrintWriter createUploadWriter(String outdir) throws IOException {
		new File(outdir).mkdirs();
		return new PrintWriter(new BufferedWriter(new FileWriter(new File(outdir, REPORT_UPLOAD_FILE_NAME))));
	}
	
	protected PrintWriter createMailWriter(String outdir) throws IOException {
		new File(outdir).mkdirs();
		return new PrintWriter(new BufferedWriter(new FileWriter(new File(outdir, REPORT_EMAIL_FILE_NAME))));
	}

	/**
	 * Creates a table showing the highlights of each test method with links to
	 * the method details
	 */
	protected void upload_generateMethodSummaryReport(List<ISuite> suites) {
		//ArrayList<TestMethodsResult> resultArray = new ArrayList<TestMethodsResult> ();
		
		m_methodIndex = 0;
		
		//uploadWriter.println("<table cellspacing=\"0\" cellpadding=\"0\" class=\"methodOverview\" align=\"center\" width=\"100%\">");
		startResultSummaryTable("methodOverview", uploadWriter);
		int testIndex = 1;
		for (ISuite suite : suites) {
			
	
			Map<String, ISuiteResult> r = suite.getResults();
			
			int testSuiteCount = 0;
			for (ISuiteResult r2 : r.values()) {
				
				System.out.println("ISuiteResult Count = "+testSuiteCount++);
				
				ITestContext testContext = r2.getTestContext();
				String testName = testContext.getName();
				
				System.out.println("Test Context name = "+testName);
				
				
				m_testIndex = testIndex;
				
				
				String browserParameter =  testContext.getSuite().getParameter("browser");
				System.out.println("Test Context browserParameter = "+browserParameter);
				
				String browser = "";
				if(testContext.getName().contains("Firefox")){
					browser = "Firefox";
				}
				else{
					browser = "Firefox";
				}
				/*
				 *  Customised Report In this section...
				 */
				resultSummary(browser, testContext, suite, testContext.getFailedConfigurations(), testName, "failed", " (configuration methods)",uploadWriter);
				resultSummary(browser, testContext, suite, testContext.getFailedTests(), testName, "failed", "",uploadWriter);
				resultSummary(browser, testContext, suite, testContext.getSkippedConfigurations(), testName, "skipped", " (configuration methods)",uploadWriter);
				resultSummary(browser, testContext, suite, testContext.getSkippedTests(), testName, "skipped", "",uploadWriter);
				resultSummary(browser, testContext, suite, testContext.getPassedTests(), testName, "passed", "",uploadWriter);
				
				testIndex++;
			}
		}
		
		printTheResultSummary(resultArray);
	}
   
	/** Creates a section showing known results for each method */
	protected void upload_generateMethodDetailReport(List<ISuite> suites) {
		
		m_methodIndex = 0;
		for (ISuite suite : suites) {
			Map<String, ISuiteResult> r = suite.getResults();
			for (ISuiteResult r2 : r.values()) {
				ITestContext testContext = r2.getTestContext();
				if (r.values().size() > 0) {
					uploadWriter.println("<h1>" + testContext.getName() + "</h1>");
				}
				resultDetail(testContext.getFailedConfigurations(),uploadWriter);
				resultDetail(testContext.getFailedTests(),uploadWriter);
				resultDetail(testContext.getSkippedConfigurations(),uploadWriter);
				resultDetail(testContext.getSkippedTests(),uploadWriter);
				resultDetail(testContext.getPassedTests(),uploadWriter);
			}
		}
	}

	/**
	 * @param tests
	 */
	private void resultSummary(String browser, ITestContext testContext,ISuite suite, IResultMap tests, String testname,
			String style, String details,PrintWriter writer) {
		
	
		if (tests.getAllResults().size() > 0) {
			
		
			for (ITestNGMethod method : getMethodSet(tests, suite)) {
				
				boolean isMethodAlreadyExits = false;
			
				TestMethodsResult testMedhotData = new TestMethodsResult();
				
				System.out.println("browser name in result summary = "+browser);
				
				int priority  = method.getPriority(); 
				
				System.out.println("Method priority : "+priority);
				
				System.out.println("Method name : "+method);
				System.out.println("Test name : "+testname);

				testMedhotData.setMethodPriority(priority);
				
				testMedhotData.setMethod(method);
				testMedhotData.setTestContext(testContext);
				
				testMedhotData.setSuite(suite);
				testMedhotData.setResultMap(tests);
				testMedhotData.setTestName(testname);
				
				testMedhotData.setDetails(details);
				testMedhotData.setPrinter(writer);
				
				testMedhotData.setBrowserName(browser);
			    
				//testMedhotData.setStyle(style);
				
				
				testMedhotData.setStyle("passed");
				for(int resultVal = 0; resultVal<resultArray.size(); resultVal++){
					TestMethodsResult getTestMethods = resultArray.get(resultVal);
					String prevMethodName =  getTestMethods.getMethod().getMethodName();
					String currentMethodName =  method.getMethodName();
					if (prevMethodName.equals(currentMethodName)) {
						isMethodAlreadyExits = true;
						
						System.out.println("Browser concat = "+getTestMethods.getBrowserName()+ " , " + browser);
						
						
						System.out.println("Method already exists, so not add it in report. Just concatenate the browser name.");
						break;
					} else {
						
					}
				}
				
				if (!isMethodAlreadyExits) {
				    String excel_MethodName = "";
				    //String excel_methodSuccessOrNot = "";
				    int excel_priority = 0;
				    String executeOrNot = null;
				    String excel_methodExceptionOrNot = "";
				    boolean excel_methodSuccessOrNot = false;
				    
				    
				    Object methodResult = testContext.getAttribute(method.getMethodName());
				    System.out.println("methodResult object :  "+methodResult );
				    if(methodResult !=null){
				    	
						@SuppressWarnings("unchecked")
						HashMap<String, Object> resultMap= (HashMap<String, Object>) methodResult;
						if(resultMap!=null){
							
							
							
							excel_MethodName = (String) resultMap.get(TEST_RESULT.R_METHOD_NAME);
							executeOrNot  = (String) resultMap.get(TEST_RESULT.R_METHOD_EXECUTE_OR_NOT); 
							//resulMap.put(TEST_RESULT.R_IS_SUCCESS, TEST_RESULT.R_IS_EXCEPTION);
							excel_methodExceptionOrNot  = (String) resultMap.get(TEST_RESULT.R_MESSAGE); 
							
							Object boolObject = resultMap.get(TEST_RESULT.R_IS_SUCCESS);
							if( boolObject !=null && boolObject instanceof Boolean){
								excel_methodSuccessOrNot  = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS); 
							}
							
							
							if(excel_MethodName!=null){
								System.out.println("Method name "+excel_MethodName );
								System.out.println("Method exe or not "+executeOrNot);
								//System.out.println("Method pass or fail "+excel_methodSuccessOrNot);
							}
							else{
								System.out.println("Result details are null ");
							}
							
						}
						else{
							System.out.println("Result map is null ");
						}
						
						if(executeOrNot!=null && excel_methodExceptionOrNot!=TEST_RESULT.R_IS_EXCEPTION 
								&& excel_methodSuccessOrNot){
							if(executeOrNot.equalsIgnoreCase("Y")){
								
								resultArray.add(testMedhotData); 
							}
							else{
								System.out.println("executeOrNot is N. So this methos should not display in report :  "+method.getMethodName());
							}
						}
						else{
							
							testMedhotData.setStyle("failed");
					    	resultArray.add(testMedhotData); 
					    	resultFailedArray.add(testMedhotData);
					    	System.out.println("Mark this method as failed "+method.getMethodName() +" :::: excel_MethodName= "+excel_MethodName);
							
						}
				    }
				    else{
				    	System.out.println("Attribut may be not set for metho name:  "+method.getMethodName() );
				    }
			} 
		}
			
			
		}
		
		
	}
    
	
	
	private void printTheResultSummary(ArrayList<TestMethodsResult> resultArray){

		
		String lastClassName = "";
		int mq = 0;
		int cq = 0;
		
		
		   
		for (TestMethodsResult testMethod: resultArray) {
			    //ITestNGMethod method : methodCollections
			    
			    System.out.println(testMethod);
			    int methodPriority = testMethod.getMethodPriority();
			    System.out.println("Method priority in order : "+methodPriority);
			    
			    ITestNGMethod method = testMethod.getMethod(); 
			    ITestContext testContext = testMethod.getTestContext(); 
			    ISuite suite = testMethod.getSuite(); 
			    IResultMap tests = testMethod.getResultMap(); 
			    String testname = testMethod.getTestName(); 
			    String style = testMethod.getStyle(); 
			    String details = testMethod.getDetails(); 
			    PrintWriter writer = testMethod.getPrinter(); 
			    String browserName = testMethod.getBrowserName(); 

			    Object methodResult = testContext.getAttribute(method.getMethodName());
			    
			    System.out.println("style value =  : "+style);
			    String[] browserArray  = browserName.split(",");
			    String[] styleArray  = style.split(",");
			    
			    int noOfBrowsers  = browserArray.length; 
			    
				StringBuffer buff = new StringBuffer();
				
				m_row += 1;
				m_methodIndex += 1;
				ITestClass testClass = method.getTestClass();
				String className = testClass.getName();				
				
				Set<ITestResult> resultSet = tests.getResults(method);
				long end = Long.MIN_VALUE;
				long start = Long.MAX_VALUE;
				long startMS=0;
						
				String actualResult  = "No";
				String expectedResult = "No";
				
				for (ITestResult testResult : tests.getResults(method)) {
					if (testResult.getEndMillis() > end) {
						end = testResult.getEndMillis()/1000;
					}
					if (testResult.getStartMillis() < start) {
						startMS = testResult.getStartMillis();
						start =startMS/1000;
					}
					
					Throwable exception=testResult.getThrowable();
					boolean hasThrowable = exception != null;
					if(hasThrowable){
						String str = Utils.stackTrace(exception, true)[0];
						Scanner scanner = new Scanner(str);
						actualResult = scanner.nextLine();
						
						
					}
				}
				DateFormat formatter = new SimpleDateFormat("hh:mm:ss");
				Calendar calendar = Calendar.getInstance();
			    calendar.setTimeInMillis(startMS);
		
				mq += 1;
				if (mq > 1) {
					//buff.append("<tr style=\"background-color:white\">");
				}
				String description = method.getDescription();
				
				String testInstanceName = resultSet
						.toArray(new ITestResult[] {})[0].getTestName();
				
		
				if(actualResult.contains("java.lang.AssertionError:")){
					actualResult= actualResult.replace("java.lang.AssertionError:","");
				}
				
				if(browserName.length()==0){
					browserName= "Please check browser value";
				}
				
				
				System.out.println("browser name  in Report= "+browserName);
				
				
				
				actualResult = (style == "failed" ? actualResult : actualResult);
				
				
				
				String excel_MethodName = "No Method Name in Excel";
				
				String excel_expectedResult = "No Expected Result";
				String excel_actualResult = "No Actual Result";
				
				String excel_methodExceptionOrNot = "No Exception Result";
				
				String excel_methodExceptionMessage = "No Exception Message Result";
				
				boolean isMethodSuccOrNot = false;
				
				
				System.out.println("excel_testInstanceName = "+method.getMethodName());
				System.out.println("excel_methodResult = "+methodResult);
				
			
				
				if(methodResult !=null){
					@SuppressWarnings("unchecked")
					HashMap<String, Object> resultMap= (HashMap<String, Object>) methodResult;
					if(resultMap!=null){
						
						excel_MethodName = (String) resultMap.get(TEST_RESULT.R_METHOD_NAME);
						
						excel_expectedResult = (String) resultMap.get(TEST_RESULT.R_EXPECTED_RESULT);
						excel_actualResult = (String) resultMap.get(TEST_RESULT.R_ACTUAL_RESULT);
						
						
						
						description = (String) resultMap.get(TEST_RESULT.R_DESRIPTION);
					
						System.out.println("excel_MethodName = "+excel_MethodName);
						
						System.out.println("excel_expectedResult = "+excel_expectedResult);
						
						expectedResult = excel_expectedResult;
						
						

						excel_methodExceptionOrNot  = (String) resultMap.get(TEST_RESULT.R_MESSAGE); 
	
						if(excel_methodExceptionOrNot!=null){
							
							if(excel_methodExceptionOrNot.equals(TEST_RESULT.R_IS_EXCEPTION)){
								excel_methodExceptionMessage  = (String) resultMap.get(TEST_RESULT.R_EXCEPTION_ERROR_MESSAGE); 
								actualResult = excel_methodExceptionMessage;
							}
							else{
								actualResult = excel_methodExceptionOrNot;
								
								System.out.println("excel_methodExceptionOrNot value = "+excel_methodExceptionOrNot);
								System.out.println("excel_methodExceptionOrNot not  null = and not equal to exceptio ");
							}
							
						}
						else{
							
							if(resultMap.get(TEST_RESULT.R_IS_SUCCESS)!=null){
								
								isMethodSuccOrNot  = (boolean) resultMap.get(TEST_RESULT.R_IS_SUCCESS); 
								if(isMethodSuccOrNot){
									actualResult = excel_actualResult;
								}
								else{
									actualResult = excel_methodExceptionOrNot;
								}
							}
							else{
								System.out.println("resultMap.get(TEST_RESULT.R_IS_SUCCESS) null = "+resultMap);
							}
							
							
						}
					
					}
				}
				
				/*
				 *  Custom Values to be displayed in method
				 */
						buff.append("<td style=\"color:black;\" rowspan=\""+noOfBrowsers+"\">"
						
						+ "<b>"+excel_MethodName+"</b>"
						+ (description != null && description.length() > 0 ? "" : "")
								
								+ (null == testInstanceName ? "" : "<br>("
										+ testInstanceName + ")") + "</td>"
										+ "<td class=\"numi\" style=\"text-align:left;padding-right:2em\" color=\"black\" rowspan=\""+noOfBrowsers+"\">" + "<p>"+ (description != null && description.length() > 0 ? description : "")+"</p>"+"<br/></td>"
										+ "<td style=\"text-align:left\" color=\"black\" rowspan=\""+noOfBrowsers+"\">" + "<p>"+ expectedResult + "</p>"+"</td>" 
										+ "<td style=\"text-align:left\" color=\"black\" rowspan=\""+noOfBrowsers+"\">" + "<p>"+ actualResult  + "</p>"+"</td>"); 
										
						
										for(int browserCount = 0 ; browserCount<1; browserCount++){
											String statusVal = styleArray[browserCount].trim();
											System.out.println("syle-array- 0= "+statusVal);
											String PassOrFailCase = (statusVal.contains("failed") ? "Failed" : "Passed");
											buff.append("<td class=\"numi\">"+ "<p>"+browserArray[0]+"</p>" + "</td>"
										             + "<td class=\"numi\">"+ "<p style=\""+(statusVal.contains("failed")? "color:red" : "color:green")+"\">"+PassOrFailCase+"</p>" + "</td>"
										             + "</tr>");
										}
										
										
										for(int browserCount = 1 ; browserCount<browserArray.length; browserCount++){
											String statusVal = styleArray[browserCount].trim();
											System.out.println("syle-array- >1= "+statusVal);
											String PassOrFailCaseer = ( statusVal.contains("failed") ? "Failed" : "Passed");
											buff.append("<tr>"
							                + "<td class=\"numi\">"+ "<p>"+browserArray[browserCount]+"</p>" + "</td>"
							                + "<td class=\"numi\">"+ "<p style=\""+(statusVal.contains("failed") ? "color:red" : "color:green")+"\">"+PassOrFailCaseer+"</p>" + "</td>"
											+"</tr>");
										}
						 
							
							writer.print("<tr>" + "<td rowspan=\""+noOfBrowsers+"\">");
							writer.println(+ m_methodIndex + "</td>" + buff);
			
		}
		

		uploadWriter.println("</table>");
		//shnew
		uploadWriter.println("</table>");
	}
	
	private String timeConversion(long seconds) {

	    final int MINUTES_IN_AN_HOUR = 60;
	    final int SECONDS_IN_A_MINUTE = 60;

	    int minutes = (int) (seconds / SECONDS_IN_A_MINUTE);
	    seconds -= minutes * SECONDS_IN_A_MINUTE;

	    int hours = minutes / MINUTES_IN_AN_HOUR;
	    minutes -= hours * MINUTES_IN_AN_HOUR;

	    return prefixZeroToDigit(hours) + ":" + prefixZeroToDigit(minutes) + ":" + prefixZeroToDigit((int)seconds);
	}
	
	private String prefixZeroToDigit(int num){
		int number=num;
		if(number<=9){
			String sNumber="0"+number;
			return sNumber;
		}
		else
			return ""+number;
		
	}
	/** Starts and defines View summary report link */
	private void mail_viewSummaryReportInAlink(String style) {
		mailWriter.println("<tr><td colspan=\"8\" align=\"center\"><h3>");
		mailWriter.println("<a href="+"\"http://demo.shenll.net/MoneySmartTesting/"+REPORT_UPLOAD_FILE_NAME+"\""+">Click Here to view the full test report</a></h3></td></tr>");
		mailWriter.println("</table>");
		
	}

	/** Starts and defines columns result summary table */
	private void startResultSummaryTable(String style,PrintWriter writer) {
		tableStart(style, "summary",writer);
		uploadWriter.println("<tr><th>#</th>"
				+ "<th>Test Case Title</th><th>Test Case Description</th><th>Expected Result</th><th>Actual Result</th><th>Browser</th><th>Status</th>");
		//<th>Start Time </th><th>Execution Time<br/>(hh:mm:ss)</th></tr>
		m_row = 0;
	}

	private String qualifiedName(ITestNGMethod method) {
		StringBuilder addon = new StringBuilder();
		String[] groups = method.getGroups();
		int length = groups.length;
		if (length > 0 && !"basic".equalsIgnoreCase(groups[0])) {
			addon.append("(");
			for (int i = 0; i < length; i++) {
				if (i > 0) {
					addon.append(", ");
				}
				addon.append(groups[i]);
			}
			addon.append(")");
		}

		return "<b>" + method.getMethodName() + "</b> " + addon;
	}

	private void resultDetail(IResultMap tests,PrintWriter writer) {
		Set<ITestResult> testResults=tests.getAllResults();
		List<ITestResult> testResultsList = new ArrayList<ITestResult>(testResults);
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
		System.setProperty("java.util.Collections.useLegacyMergeSort", "true");
		Collections.sort(testResultsList, new TestResultsSorter());
		for (ITestResult result : testResultsList) {
			ITestNGMethod method = result.getMethod();
			m_methodIndex++;
			String cname = method.getTestClass().getName();
			writer.println("<h2 id="+"\"m\""+m_methodIndex + "\""+ ">" + cname + ":"
					+ method.getMethodName() + "</h2>");
			Set<ITestResult> resultSet = tests.getResults(method);
			generateResult(result, method, resultSet.size(),writer);
			writer.println("<p class="+"\"totop\""+"><a href="+"\"#summary\""+">back to summary</a></p>");

		}
	}

	private void generateResult(ITestResult ans, ITestNGMethod method,
			int resultSetSize,  PrintWriter writer) {
		Object[] parameters = ans.getParameters();
		boolean hasParameters = parameters != null && parameters.length > 0;
		if (hasParameters) {
			tableStart("result", null,writer);
			writer.print("<tr class="+"\"param\""+">");
			for (int x = 1; x <= parameters.length; x++) {
				writer.print("<th>Param." + x + "</th>");
			}
			writer.println("</tr>");
			writer.print("<tr class="+"\"param stripe\""+">");
			for (Object p : parameters) {
				writer.println("<td>" + Utils.escapeHtml(Utils.toString(p))
						+ "</td>");
			}
			writer.println("</tr>");
		}
		List<String> msgs = Reporter.getOutput(ans);
		boolean hasReporterOutput = msgs.size() > 0;
		Throwable exception = ans.getThrowable();
		boolean hasThrowable = exception != null;
		if (hasReporterOutput || hasThrowable) {
			if (hasParameters) {
				writer.print("<tr><td");
				if (parameters.length > 1) {
					writer.print(" colspan="+"\"" + parameters.length + "\"");
				}
				writer.println(">");
			} else {
				writer.println("<div>");
			}
			if (hasReporterOutput) {
				if (hasThrowable) {
					writer.println("<h3>Test Messages</h3>");
				}
				for (String line : msgs) {
					writer.println(line + "<br/>");
				}
			}
			if (hasThrowable) {
				boolean wantsMinimalOutput = ans.getStatus() == ITestResult.SUCCESS;
				if (hasReporterOutput) {

					
					writer.println("<h3>"
							+ (wantsMinimalOutput ? "Expected Exception"
									: "Failure") + "</h3>");
				}
				
				//generateExceptionReport(exception, method);
			}
			if (hasParameters) {
				writer.println("</td></tr>");
			} else {
				writer.println("</div>");
			}
		}
		if (hasParameters) {
			writer.println("</table>");
		}
	}

	protected void generateExceptionReport(Throwable exception, ITestNGMethod method,PrintWriter writer ) {
		writer.print("<div class=\"stacktrace\">");
		writer.print(Utils.stackTrace(exception, true)[0]);
		writer.println("</div>");
	}

	/**
	 * Since the methods will be sorted chronologically, we want to return the
	 * ITestNGMethod from the invoked methods.
	 */
	private Collection<ITestNGMethod> getMethodSet(IResultMap tests, ISuite suite) {
		
		List<IInvokedMethod> r = Lists.newArrayList();
		List<IInvokedMethod> invokedMethods = suite.getAllInvokedMethods();
		for (IInvokedMethod im : invokedMethods) {
			if (tests.getAllMethods().contains(im.getTestMethod())) {
				r.add(im);
			}
		}
		
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
		System.setProperty("java.util.Collections.useLegacyMergeSort", "true");
		Collections.sort(r,new TestSorter());
		List<ITestNGMethod> result = Lists.newArrayList();
		
		// Add all the invoked methods
		for (IInvokedMethod m : r) {
			for (ITestNGMethod temp : result) {
				if (!temp.equals(m.getTestMethod()))
					result.add(m.getTestMethod());
			}
		}
		
		// Add all the methods that weren't invoked (e.g. skipped) that we
		// haven't added yet
		Collection<ITestNGMethod> allMethodsCollection=tests.getAllMethods();
		List<ITestNGMethod> allMethods=new ArrayList<ITestNGMethod>(allMethodsCollection);
		Collections.sort(allMethods, new TestMethodSorter());
		
		for (ITestNGMethod m : allMethods) {
			if (!result.contains(m)) {
				result.add(m);
			}
		}
		return result;
	}

	@SuppressWarnings("unused")
	public void upload_generateSuiteSummaryReport(List<ISuite> suites) {
		//tableStart("testOverview", null,uploadWriter);
		uploadWriter.print("<tr>");
		tableColumnStart("Title",uploadWriter);
		tableColumnStart("# Passed",uploadWriter);
		tableColumnStart("# Skipped",uploadWriter);
		tableColumnStart("# Failed",uploadWriter);
		//tableColumnStart("Browser",uploadWriter);
		tableColumnStart("Start<br/>Time",uploadWriter);
		tableColumnStart("End<br/>Time",uploadWriter);
		tableColumnStart("Total<br/>Time(HH:MM:SS)",uploadWriter);

		uploadWriter.println("</tr>");
		NumberFormat formatter = new DecimalFormat("#,##0.0");
		int qty_tests = 0;
		int qty_pass_m = 0;
		int qty_pass_s = 0;
		int qty_skip = 0;
		long time_start = Long.MAX_VALUE;
		int qty_fail = 0;
		long time_end = Long.MIN_VALUE;
		m_testIndex = 1;
		for (ISuite suite : suites) {
			/*if (suites.size() >= 1) {
				titleRow(suite.getName(), 10,uploadWriter);
			}*/
			Map<String, ISuiteResult> tests = suite.getResults();
			for (ISuiteResult r : tests.values()) {
				qty_tests += 1;
				ITestContext overview = r.getTestContext();
				
				startSummaryRow(overview.getName(),uploadWriter);
				int q = getMethodSet(overview.getPassedTests(), suite).size();
				qty_pass_m += q;
				summaryCell(q, Integer.MAX_VALUE,uploadWriter);
				q = getMethodSet(overview.getSkippedTests(), suite).size();
				qty_skip += q;
				summaryCell(q, 0,uploadWriter);
				q = getMethodSet(overview.getFailedTests(), suite).size();
				qty_fail += q;
				summaryCell(q, 0,uploadWriter);
				
				// Write OS and Browser
				//summaryCell(StaticVariable.BrowserVesion, true,uploadWriter);
				uploadWriter.println("</td>");
							
				SimpleDateFormat summaryFormat = new SimpleDateFormat("hh:mm:ss");
				summaryCell(summaryFormat.format(overview.getStartDate()),true,uploadWriter);				
				uploadWriter.println("</td>");
				
				summaryCell(summaryFormat.format(overview.getEndDate()),true,uploadWriter);
				uploadWriter.println("</td>");

				time_start = Math.min(overview.getStartDate().getTime(), time_start);
				time_end = Math.max(overview.getEndDate().getTime(), time_end);
				summaryCell(timeConversion((overview.getEndDate().getTime() - overview.getStartDate().getTime()) / 1000), true,uploadWriter);
				
				uploadWriter.println("</tr>");
				m_testIndex++;
			}
		}
		if (qty_tests > 25) {
			
			qty_pass_m = resultArray.size();
			qty_fail = resultFailedArray.size();
			
			uploadWriter.println("<tr class=\"total\"><td>Total</td>");
			summaryCell(qty_pass_m, Integer.MAX_VALUE,uploadWriter);
			summaryCell(qty_skip, 0,uploadWriter);
			summaryCell(qty_fail, 0,uploadWriter);
			summaryCell(" ", true,uploadWriter);
			summaryCell(" ", true,uploadWriter);
			summaryCell(" ", true,uploadWriter);
			summaryCell(timeConversion(((time_end - time_start) / 1000)), true,uploadWriter);
			uploadWriter.println("<td colspan=\"3\">&nbsp;</td></tr>");
		}
		uploadWriter.println("</table>");
	}
    
	@SuppressWarnings("unused")
	public void mail_generateSuiteSummaryReport(List<ISuite> suites) {
		
		
		mailWriter.print("<tr>");
		tableColumnStart("Title",mailWriter);
		tableColumnStart("# Passed",mailWriter);
		tableColumnStart("# Skipped",mailWriter);
		tableColumnStart("# Failed",mailWriter);
		
		tableColumnStart("Test Run Start<br/>Time",mailWriter);
		tableColumnStart("Test Run End<br/>Time",mailWriter);
		tableColumnStart("Total Time Taken for Execution<br/> (HH:MM:SS)",mailWriter);
		

		
		mailWriter.println("</tr>");
		NumberFormat formatter = new DecimalFormat("#,##0.0");
		int qty_tests = 0;
		int qty_pass_m = 0;
		//int qty_pass_s = 0;
		int qty_skip = 0;
		long time_start = Long.MAX_VALUE;
		int qty_fail = 0;
		long time_end = Long.MIN_VALUE;
		m_testIndex = 1;
		for (ISuite suite : suites) {
			
			Map<String, ISuiteResult> tests = suite.getResults();
			for (ISuiteResult r : tests.values()) {
				qty_tests += 1;
				ITestContext overview = r.getTestContext();
				
				startSummaryRow(overview.getName(),mailWriter);
				int q = getMethodSet(overview.getPassedTests(), suite).size();
				qty_pass_m += q;
				
				summaryCell((q-1), Integer.MAX_VALUE,mailWriter);
				q = getMethodSet(overview.getSkippedTests(), suite).size();
				qty_skip += q;
				summaryCell(q, 0,mailWriter);
				q = getMethodSet(overview.getFailedTests(), suite).size();
				qty_fail += q;
				summaryCell(q, 0,mailWriter);
				
				// Write OS and Browser
				
				
				mailWriter.println("</td>");
							
				SimpleDateFormat summaryFormat = new SimpleDateFormat("hh:mm:ss");
				summaryCell(summaryFormat.format(overview.getStartDate()),true,mailWriter);				
				mailWriter.println("</td>");
				
				summaryCell(summaryFormat.format(overview.getEndDate()),true,mailWriter);
				mailWriter.println("</td>");

				time_start = Math.min(overview.getStartDate().getTime(), time_start);
				time_end = Math.max(overview.getEndDate().getTime(), time_end);
				summaryCell(timeConversion((overview.getEndDate().getTime() - overview.getStartDate().getTime()) / 1000), true,mailWriter);
				
				
				mailWriter.println("</tr>");
				m_testIndex++;
			}
		}
		if (qty_tests > 25) {
			qty_pass_m = resultArray.size();
			qty_fail = resultFailedArray.size();
			mailWriter.println("<tr class=\"total\"><td>Total</td>");
			summaryCell(qty_pass_m, Integer.MAX_VALUE,mailWriter);
			summaryCell(qty_skip, 0,mailWriter);
			summaryCell(qty_fail, 0,mailWriter);
			summaryCell(" ", true,mailWriter);
			summaryCell(" ", true,mailWriter);
			summaryCell(" ", true,mailWriter);
			summaryCell(timeConversion(((time_end - time_start) / 1000)), true,mailWriter);
			mailWriter.println("<td colspan=\"3\">&nbsp;</td></tr>");
		}
		//mailWriter.println("</table>");
	}
	
	private void summaryCell(String[] val, PrintWriter writer) {
		StringBuffer b = new StringBuffer();
		for (String v : val) {
			b.append(v + " ");
		}
		summaryCell(b.toString(), true,writer);
	}

	private void summaryCell(String v, boolean isgood, PrintWriter writer) {
		
		System.out.println("pasitive = "+v+"  - "+ isgood);
		
		writer.print("<td class=\"numi\"" + (isgood ? "" : "_attn") + "\">" + v
				+ "</td>");
	}

	private void startSummaryRow(String label, PrintWriter writer) {
		m_row += 1;
		
		writer.print("<tr style=\"text-align:center;\""
				+ (m_row % 2 == 0 ? " class=\"stripe\"" : "")
				+ "><td style=\"text-align:left;padding-right:2em\"><b>" + label + "</b>" + "</td>");
		
	}

	private void summaryCell(int v, int maxexpected,PrintWriter writer) {
		summaryCell(String.valueOf(v), v <= maxexpected, writer);
	}

	private void tableStart(String cssclass, String id, PrintWriter writer) {
		writer.println("<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:100%; text-align: center;\""
				+ (cssclass != null ? " class=\"" + cssclass + "\"" :"  style=\"padding-bottom:2em;\"")
						+ (id != null ? " id=\"" + id + "\"" : "") + ">");
		
		m_row = 0;
	}


	private void tableColumnStart(String label, PrintWriter writer) {
		writer.print("<th>" + label + "</th>");
	}

	private void titleRow(String label, int cq, PrintWriter writer) {
		titleRow(label, cq, null, writer);
	}

	private void titleRow(String label, int cq, String id, PrintWriter writer) {
	
		writer.print("<tr");
		if (id != null) {
			writer.print(" id=\"\" + id + \"\"");
		}
		writer.println("><th colspan=\"\" + cq +\"\">" + label + "</th></tr>");
		m_row = 0;
	}

	protected void upload_writeReportTitle(String title) {
		
		uploadWriter.print("</td></tr>");
	}
	
	protected void mail_writeReportTitle(String title) {
		
		mailWriter.print("</td></tr>");
	}

	/*
	 * Method to get Date as String
	 */
	private String getDateAsString() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	/** Starts Upload HTML stream */
	protected void startHtml(PrintWriter out) {
		out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
		out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		out.println("<head>");
		out.println("<title>"+ChannelName+" Channel Test Report</title>");
		out.println("<style type=\"text/css\">");
		out.println(".body{font-family: 'ProximaNova', Helvetica, Arial;}");
		out.println("table {margin-bottom:10px;border-collapse:collapse;empty-cells:show}");
		out.println("td,th {border:1px solid #34495e;padding:.25em .5em; color:#7f8c8d;}");
		out.println(".result th {vertical-align:bottom}");
		out.println(".param th {padding-left:1em;padding-right:1em}");
		out.println(".param td {padding-left:.5em;padding-right:2em}");
		out.println(".stripe td,.stripe th {background-color: #E6EBF9}");
		out.println(".numi,.numi_attn {text-align:center}");
		out.println(".total td {font-weight:bold}");
		out.println(".passedodd td {background-color: #0A0}");
		out.println(".passedeven td {background-color: #3F3}");
		out.println(".skippedodd td {background-color: #CCC}");
		out.println(".skippedodd td {background-color: #DDD}");
		out.println(".failedodd td,.numi_attn {background-color: #F33}");
		out.println(".failedeven td,.stripe .numi_attn {background-color: #D00}");
		out.println(".stacktrace {white-space:pre;}");
		out.println(".totop {font-size:85%;text-align:center;border-bottom:2px solid #000}");
		
		out.println(".test-header{font-size: 20px;font-weight:bold;vertical-align:center;background: #34495E;color:white;}");
		out.println(".test-title{margin-left:50px;}");
		out.println(".navlogo{margin-top:20px;}");
		
		out.println("</style>");
		out.println("</head>");
		out.println("<body>");
		
		out.println("<table cellspacing=\"0\" cellpadding=\"0\" class=\"testOverview\" align=\"center\" width=\"100%\">");
		out.println("<tr>");
		
		
		out.println("<td colspan=\"8\" class=\"test-header\">");
				out.println("<table style=\"border:none;  cellspacing=\"0\" cellpadding=\"0\">");
						out.println("<tbody><tr><td style=\"width:10%\" border=\"0 none\"><img alt=\"moneysmart.sg\" class=\"pull-left navlogo\" src=\"http://www.moneysmart.sg/assets/navigation_logo-63bc64931bad14ce7d73578404d85bd5.png\">");
							out.println("</td>");
									out.println("<td class=\"test-header\" style=\"text-align:center;\" border=\"0 none\"><span class=\"test-title\"> "+ChannelName+" Channel Test Report (# "+uniqueReportId+") - "+getDateAsString()+"</span></td>");
									out.println("</tr>");
											out.println("</tbody></table>");
													out.println("</td>");
		
		
	}
	
	/** Finishes HTML stream */
	protected void endHtml(PrintWriter out) {
		
		out.println("</body></html>");
	}

	// ~ Inner Classes --------------------------------------------------------
	/** Arranges methods by classname and method name */
	private class TestSorter implements Comparator<IInvokedMethod> {
		// ~ Methods
		// -------------------------------------------------------------

		/** Arranges methods by classname and method name */
		@Override
		public int compare(IInvokedMethod obj1, IInvokedMethod obj2) {
			int r = obj1.getTestMethod().getTestClass().getName().compareTo(obj2.getTestMethod().getTestClass().getName());
			return r;
		}
	}
	
	private class TestMethodSorter implements Comparator<ITestNGMethod> {
		@Override
		public int compare(ITestNGMethod obj1, ITestNGMethod obj2) {
			int r = obj1.getTestClass().getName().compareTo(obj2.getTestClass().getName());
			if (r == 0) {
				r = obj1.getMethodName().compareTo(obj2.getMethodName());
			}
			return r;
		}
	}

	private class TestResultsSorter implements Comparator<ITestResult> {
		@Override
		public int compare(ITestResult obj1, ITestResult obj2) {
			int result = obj1.getTestClass().getName().compareTo(obj2.getTestClass().getName());
			if (result == 0) {
				result = obj1.getMethod().getMethodName().compareTo(obj2.getMethod().getMethodName());
			}
			return result;
		}
	}
	
	
	public String genRandomUniqueNumber() {
	    Random r = new Random( System.currentTimeMillis() );
	    return ""+(10000 + r.nextInt(20000));
	}

	/*
	 * Listeners for Upload Task. Whether the generated file is uploaded to server or not
	 * @see com.common.utility.FTPFileUploadListener#uploadTaskSuccessListener(int, java.lang.String)
	 */
	@Override
	public void uploadTaskSuccessListener(int statusCode, String statusMessage) {
		// TODO Auto-generated method stub
		System.out.println("Upload Success statusCode...." + statusCode);
		System.out.println("Upload Success status...." + statusMessage);
	
		/*
		 * Start email sending task after report generated
		 */
		SendEmailTask email = new SendEmailTask();
		emailSendListener = this;
		email.send(emailSendListener,
				REPORT_EMAIL_FILE_NAME,
				MAIL_FROM_EMAIL_ID,MAIL_FROM_EMAIL_PASSWORD,
				MAIL_FROM_USERNAME, MAIL_TO_USERNAME,
				MAIL_FROM_EMAIL_ID, MAIL_TO_EMAIL_ID,
				MAIL_SUBJECT, MAIL_BODY_MESSAGE,MAIL_CC_USER,MAIL_CC_EMAIL);
	}

	@Override
	public void uploadTaskFailureListener(int statusCode, String statusMessage) {
		// TODO Auto-generated method stub
		System.out.println("Upload Failure statusCode...." + statusCode);
		System.out.println("Upload Failure status...." + statusMessage);
	}

	/*
	 * Listeners for Email Task. Whether the uploaded file is send to the Reporting email or not
	 * @see com.common.utility.SendEmailTaskListener#sendEmailSuccessListener(int, java.lang.String)
	 */
	@Override
	public void sendEmailSuccessListener(int statusCode, String statusMessage) {
		// TODO Auto-generated method stub
		System.out.println("Email Success statusCode...." + statusCode);
		System.out.println("Email Success status...." + statusMessage);
	}

	@Override
	public void sendEmailFailureListener(int statusCode, String statusMessage) {
		// TODO Auto-generated method stub
		System.out.println("Email Failure statusCode...." + statusCode);
		System.out.println("Email Failure status...." + statusMessage);
		
		/*
		 * Test Run completed...
		 */
		System.out.println("Test Run Completed...." + statusMessage);
	}
		
}





