package com.testing.utility;
/**
 * 
 * @author Shenll Technologies Solutions
 *
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GetWebElement {

	private String webElementTypeIsId = "id";
	private String webElementTypeIsClassName = "className";
	private String webElementTypeIsXPath = "xpath";
	private String webElementTypeIsCSSSelector = "cssSelector";
	private String webElementTypeIsName = "name";
	private String webElementTypeIsLinkText = "linkText";
	
	public WebElement getWebElemntFromWebElemntType(WebDriver driver,
			String strWebElementLocator, String webElement) throws Exception{
		
         WebElement finalWebElement = null; 
         
         if(strWebElementLocator.equals(webElementTypeIsId)){
        	 finalWebElement = driver.findElement(By.id(webElement));
         }
         else if(strWebElementLocator.equals(webElementTypeIsClassName))
         {
        	 finalWebElement = driver.findElement(By.className(webElement));	
         }
         else if(strWebElementLocator.equals(webElementTypeIsXPath))
         {
        	 finalWebElement = driver.findElement(By.xpath(webElement));
         }
         else if(strWebElementLocator.equals(webElementTypeIsCSSSelector))
         {
        	 finalWebElement =driver.findElement(By.cssSelector(webElement));	
         }
         else if(strWebElementLocator.equals(webElementTypeIsName))
         {
        	 finalWebElement =driver.findElement(By.name(webElement));
         }
         else if(strWebElementLocator.equals(webElementTypeIsLinkText))
         {
        	 finalWebElement = driver.findElement(getWeblementByUsingLinkText(webElement));
         }
         
		return finalWebElement;
	}
	
	public WebElement getWebElemntFromParentElemntType(WebElement parentWebElement,
			String strWebElementLocator, String webElement) throws Exception{
		
         WebElement finalWebElement = null; 
         
         if(strWebElementLocator.equals(webElementTypeIsId)){
        	 finalWebElement = parentWebElement.findElement(By.id(webElement));
         }
         else if(strWebElementLocator.equals(webElementTypeIsClassName))
         {
        	 finalWebElement = parentWebElement.findElement(By.className(webElement));	
         }
         else if(strWebElementLocator.equals(webElementTypeIsXPath))
         {
        	 finalWebElement = parentWebElement.findElement(By.xpath(webElement));
         }
         else if(strWebElementLocator.equals(webElementTypeIsCSSSelector))
         {
        	 finalWebElement =parentWebElement.findElement(By.cssSelector(webElement));	
         }
         else if(strWebElementLocator.equals(webElementTypeIsName))
         {
        	 finalWebElement =parentWebElement.findElement(By.name(webElement));
         }
         else if(strWebElementLocator.equals(webElementTypeIsLinkText))
         {
        	 finalWebElement = parentWebElement.findElement(getWeblementByUsingLinkText(webElement));
         }
         
		return finalWebElement;
	}
	
	private By getWeblementByUsingLinkText(String webElementLocator){
		By byElementUsingLinkText = By.linkText(webElementLocator);
		return byElementUsingLinkText;
	}
}
