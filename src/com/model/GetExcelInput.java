package com.model;
/**
* 
* @author Shenll Technology Solutions
*
*/
import java.util.ArrayList;
import java.util.HashMap;

import com.model.ExcelInputData;

public class GetExcelInput {

	public MethodInputData getAllInputOf_A_Method(String methodName){
		MethodInputData methodInput = null;
		ExcelInputData excelInputData = ExcelInputData.getInstance();
        ArrayList<HashMap<String, MethodInputData>> excelValues = excelInputData.getExcelInput();
		for(int excelInputCount = 0; excelInputCount<excelValues.size();excelInputCount++){
			 HashMap<String, MethodInputData> methodInputMap = excelValues.get(excelInputCount);
			 if(methodInputMap.get(methodName)!=null){
				 methodInput = methodInputMap.get(methodName);
				 break;
			 }
		}
		return methodInput;
	}
	
	public String get_A_Value_Using_Key_Of_A_Method(String methodName, String key){
		String valueForKey = "";
		ExcelInputData excelInputData = ExcelInputData.getInstance();
        ArrayList<HashMap<String, MethodInputData>> excelValues = excelInputData.getExcelInput();
		for(int excelInputCount = 0; excelInputCount<excelValues.size();excelInputCount++){
			 HashMap<String, MethodInputData> methodInputMap = excelValues.get(excelInputCount);
			 MethodInputData methodInput = methodInputMap.get(methodName);
			 if(methodInput!=null){
				 HashMap<String,String> allInputOfAMethod  = methodInput.getMethodInput();
				 if(allInputOfAMethod!=null && allInputOfAMethod.get(key)!=null){
					 valueForKey = allInputOfAMethod.get(key);
					 break;
				 }
			 }
		}
		return valueForKey;
	}
	
	/*
	 *  Get Testimonial data only
	 */
	
	public String get_A_Value_Using_Key_Of_Testimonial_Method(String methodName, String key){
		String valueForKey = "";
		ExcelInputData excelInputData = ExcelInputData.getInstance();
        ArrayList<HashMap<String, ExcelTestimonialData>> excelValues = excelInputData.getExcelTestimonialInputData();
		for(int excelInputCount = 0; excelInputCount<excelValues.size();excelInputCount++){
			 HashMap<String, ExcelTestimonialData> methodInputMap = excelValues.get(excelInputCount);
			 ExcelTestimonialData methodInput = methodInputMap.get(methodName);
			 if(methodInput!=null){
				 HashMap<String,String> allInputOfAMethod  = methodInput.getMethodInput();
				 
				 if(allInputOfAMethod!=null && allInputOfAMethod.get(key)!=null){
					 valueForKey = allInputOfAMethod.get(key);
					 break;
				 }
			 }
		}
		return valueForKey;
	}
	
	/*
	 *  Get Icon data only
	 */
	
	public String get_A_Value_Using_Key_Of_Icon_Method(String methodName, String key){
		String valueForKey = "";
		ExcelInputData excelInputData = ExcelInputData.getInstance();
        ArrayList<HashMap<String, ExcelIconInputData>> excelValues = excelInputData.getExcelIconInputData();
		for(int excelInputCount = 0; excelInputCount<excelValues.size();excelInputCount++){
			 HashMap<String, ExcelIconInputData> methodInputMap = excelValues.get(excelInputCount);
			 ExcelIconInputData methodInput = methodInputMap.get(methodName);
			 if(methodInput!=null){
				 HashMap<String,String> allInputOfAMethod  = methodInput.getMethodInput();
				 
				 if(allInputOfAMethod!=null && allInputOfAMethod.get(key)!=null){
					 valueForKey = allInputOfAMethod.get(key);
					 break;
				 }
			 }
		}
		return valueForKey;
	}
	
	
	/*
	 *  Get Filters data only
	 */
	
	public String get_A_Value_Using_Key_Of_Filters_Method(String methodName, String key){
		String valueForKey = null;
		ExcelInputData excelInputData = ExcelInputData.getInstance();
        ArrayList<HashMap<String, ExcelFilterInputData>> excelValues = excelInputData.getExcelFilterInputData();
		for(int excelInputCount = 0; excelInputCount<excelValues.size();excelInputCount++){
			 HashMap<String, ExcelFilterInputData> methodInputMap = excelValues.get(excelInputCount);
			 ExcelFilterInputData methodInput = methodInputMap.get(methodName);
			 if(methodInput!=null){
				 HashMap<String,String> allInputOfAMethod  = methodInput.getMethodInput();
				 
				 if(allInputOfAMethod!=null && allInputOfAMethod.get(key)!=null){
					 valueForKey = allInputOfAMethod.get(key);
					 break;
				 }
			 }
		}
		return valueForKey;
	}
	
	/*
	 *  Get Filters Reverse Key data only
	 */
	
	/*public String get_A_KEY_Using_Value_Of_Filters_Method(String keyNameToFindMethodName, String valueContains){
		String valueForKey = "";
		ExcelInputData excelInputData = ExcelInputData.getInstance();
        ArrayList<HashMap<String, ExcelFilterInputData>> excelValues = excelInputData.getExcelFilterInputData();
		for(int excelInputCount = 0; excelInputCount<excelValues.size();excelInputCount++){
			 HashMap<String, ExcelFilterInputData> methodInputMap = excelValues.get(excelInputCount);
			 
			 for(Object getExcelObjectFroSet:methodInputMap.keySet()){
				 
				 ExcelFilterInputData methodInput = (ExcelFilterInputData)getExcelObjectFroSet;
				 
				 if(methodInput!=null){
					 HashMap<String,String> allInputOfAMethod  = methodInput.getMethodInput();
					 
					 if(allInputOfAMethod!=null && allInputOfAMethod.get(key)!=null){
						 valueForKey = allInputOfAMethod.get(key);
						 break;
					 }
				 }
			 }
			 
		}
		return valueForKey;
	}*/
	
	/*
	 *  Get Product Validation data only
	 */
	
	public String get_A_Value_Using_Key_Of_ProductValidation_Method(String methodName, String key){
		String valueForKey = "";
		ExcelInputData excelInputData = ExcelInputData.getInstance();
        ArrayList<HashMap<String, ExcelProductValidationInputData>> excelValues = excelInputData.getExcelProductValidationInputData();
		for(int excelInputCount = 0; excelInputCount<excelValues.size();excelInputCount++){
			 HashMap<String, ExcelProductValidationInputData> methodInputMap = excelValues.get(excelInputCount);
			 ExcelProductValidationInputData methodInput = methodInputMap.get(methodName);
			 if(methodInput!=null){
				 HashMap<String,String> allInputOfAMethod  = methodInput.getMethodInput();
				 
				 if(allInputOfAMethod!=null && allInputOfAMethod.get(key)!=null){
					 valueForKey = allInputOfAMethod.get(key);
					 break;
				 }
			 }
		}
		return valueForKey;
	}
}
