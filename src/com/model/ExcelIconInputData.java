package com.model;
/**
* 
* @author Shenll Technology Solutions
*
*/
import java.util.HashMap;

public class ExcelIconInputData {
	public HashMap<String,String> iconInput; 
	
	public ExcelIconInputData(){
		this.iconInput = new HashMap<String,String>();
	}
	
	public void setMethodInput(String key, String value){
		this.iconInput.put(key, value);
	}
	
	public HashMap<String,String> getMethodInput(){
		return iconInput;
	}
}
