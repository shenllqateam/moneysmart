package com.model;

/**
* 
* @author Shenll Technology Solutions
*
*/
import java.util.ArrayList;
import java.util.HashMap;

public class CommonMethods {

	public ArrayList<ArrayList<HashMap<String, String>>> readExcelData(ArrayList<String> calculationDataHeaders,
			int calculateFromCount, int calculateToCount) {
	       // Get the data from excel file
		ArrayList<ArrayList<HashMap<String, String>>> allCalculationHeader = new ArrayList<ArrayList<HashMap<String, String>>>();
		   
	       for (int rowCnt = calculateFromCount; rowCnt < calculateToCount; rowCnt++) {
	    	   ArrayList<HashMap<String, String>> singleCalculationHeader = new ArrayList<HashMap<String, String>>();
	    	   for(int caluationHeaderCount = 0; caluationHeaderCount<calculationDataHeaders.size(); caluationHeaderCount++){
	    		   HashMap<String, String> headerValueMap = new HashMap<String, String>();
	    		   headerValueMap.put(calculationDataHeaders.get(caluationHeaderCount), 
	    				   ExcelReader.ReadCell(ExcelReader.GetCell(calculationDataHeaders.get(caluationHeaderCount)), rowCnt));
	    		   singleCalculationHeader.add(headerValueMap); 
	    	   }
	    	   
	    	   allCalculationHeader.add(singleCalculationHeader);
	       }
	       return allCalculationHeader;
	 }
	
	
	public HashMap<String, ArrayList<String>> readExcelDataWithRespectToHeaders(ArrayList<String> calculationDataHeaders,
			int calculateFromCount, int calculateToCount) {
	       // Get the data from excel file
		   HashMap<String, ArrayList<String>> allCalculationHeader = new HashMap<String, ArrayList<String>>();
		   
		   for(int caluationHeaderCount = 0; caluationHeaderCount<calculationDataHeaders.size(); caluationHeaderCount++){
			   ArrayList<String> singleCalculationHeader = new ArrayList<String>();	   
	    	   for (int rowCnt = calculateFromCount; rowCnt < calculateToCount; rowCnt++) {
	    		   String singleCellValue = ExcelReader.ReadCell(ExcelReader.GetCell(calculationDataHeaders.get(caluationHeaderCount)), rowCnt);
	    		   
	    		   System.out.println("===\n singleCellValue :  "+singleCellValue);
	    		   
	    		   singleCalculationHeader.add(singleCellValue); 
	    	   }
	    	   
	    	   System.out.println("===\n allCalculationHeader :  "+allCalculationHeader);
	    	   System.out.println("===\n allCalculationHeader :  "+calculationDataHeaders.get(caluationHeaderCount));
	    	   
	    	   allCalculationHeader.put(calculationDataHeaders.get(caluationHeaderCount), singleCalculationHeader);
	       }
	       return allCalculationHeader;
	 }
}
