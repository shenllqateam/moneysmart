package com.model;

/**
* 
* @author Shenll Technology Solutions
*
*/
import java.util.HashMap;

public class ExcelFilterInputData {
	public HashMap<String,String> filterInput; 
	
	public ExcelFilterInputData(){
		this.filterInput = new HashMap<String,String>();
	}
	
	public void setMethodInput(String key, String value){
		this.filterInput.put(key, value);
	}
	
	public HashMap<String,String> getMethodInput(){
		return filterInput;
	}
}
