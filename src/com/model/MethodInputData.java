package com.model;
/**
* 
* @author Shenll Technology Solutions
*
*/
import java.util.HashMap;

public class MethodInputData {

	public HashMap<String,String> methodInput; 
	
	public MethodInputData(){
		this.methodInput = new HashMap<String,String>();
	}
	
	public void setMethodInput(String key, String value){
		this.methodInput.put(key, value);
	}
	
	public HashMap<String,String> getMethodInput(){
		return methodInput;
	}
}
