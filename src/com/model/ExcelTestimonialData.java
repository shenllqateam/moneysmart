package com.model;
/**
* 
* @author Shenll Technology Solutions
*
*/
import java.util.HashMap;

public class ExcelTestimonialData {
	public HashMap<String,String> testimonialInput; 
	
	public ExcelTestimonialData(){
		this.testimonialInput = new HashMap<String,String>();
	}
	
	public void setMethodInput(String key, String value){
		this.testimonialInput.put(key, value);
	}
	
	public HashMap<String,String> getMethodInput(){
		return testimonialInput;
	}
}
