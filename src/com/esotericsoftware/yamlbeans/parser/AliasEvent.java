

package com.esotericsoftware.yamlbeans.parser;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */

public class AliasEvent extends NodeEvent {
	public AliasEvent (String anchor) {
		super(EventType.ALIAS, anchor);
	}

	public String toString () {
		return "<" + type + " anchor='" + anchor + "'>";
	}
}
