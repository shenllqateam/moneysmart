package com.esotericsoftware.yamlbeans.parser;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */

public abstract class NodeEvent extends Event {
	public final String anchor;

	public NodeEvent (EventType eventType, String anchor) {
		super(eventType);
		this.anchor = anchor;
	}
}
