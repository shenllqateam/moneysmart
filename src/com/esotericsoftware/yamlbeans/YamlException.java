

package com.esotericsoftware.yamlbeans;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */
import java.io.IOException;


public class YamlException extends IOException {
	public YamlException () {
		super();
	}

	public YamlException (String message, Throwable cause) {
		super(message);
		initCause(cause);
	}

	public YamlException (String message) {
		super(message);
	}

	public YamlException (Throwable cause) {
		initCause(cause);
	}
}
