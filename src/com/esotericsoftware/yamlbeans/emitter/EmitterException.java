

package com.esotericsoftware.yamlbeans.emitter;

/**
 * 
 * @author Shenll Technology Solutions
 *
 */

public class EmitterException extends RuntimeException {
	public EmitterException (String message) {
		super(message);
	}
}
