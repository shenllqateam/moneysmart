package com.esotericsoftware.yamlbeans.scalar;
/**
 * 
 * @author Shenll Technology Solutions
 *
 */
import com.esotericsoftware.yamlbeans.YamlException;

import java.text.ParseException;
import java.util.Date;


public class DateSerializer implements ScalarSerializer<Date> {
	private DateTimeParser dateParser = new DateTimeParser();

	public Date read (String value) throws YamlException {
		try {
			return dateParser.parse(value);
		} catch (ParseException ex) {
			throw new YamlException("Invalid date: " + value, ex);
		}
	}

	public String write (Date object) throws YamlException {
		return dateParser.format(object);
	}
}
