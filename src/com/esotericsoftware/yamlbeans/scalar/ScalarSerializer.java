
package com.esotericsoftware.yamlbeans.scalar;
/**
 * 
 * @author Shenll Technology Solutions
 *
 */
import com.esotericsoftware.yamlbeans.YamlException;


public interface ScalarSerializer<T> {
	abstract public String write (T object) throws YamlException;

	abstract public T read (String value) throws YamlException;
}
